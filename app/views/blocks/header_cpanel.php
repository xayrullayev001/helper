<?php

use yii\helpers\Url;
use app\models\Exchange;

//$notification = \app\models\Notification::forUser(Yii::$app->user->identity->getId());

echo \app\widgets\Toast::widget([
    'options' => [
        'success' => 'teal',
        'error' => 'deep-orange darken-3',
        'warning' => 'amber accent-4',
        'info' => 'blue'
    ]
]);

$user = Yii::$app->user;

?>
<header class="mn-header navbar-fixed">
    <nav class="cyan darken-1">
        <div class="nav-wrapper row">
            <section class="material-design-hamburger navigation-toggle">
                <a href="#" data-activates="slide-out"
                   class="button-collapse show-on-large material-design-hamburger__icon">
                    <span class="material-design-hamburger__layer"></span>
                </a>
            </section>
            <div class="header-title col s3">
                <a href="<?= Url::to("/cpanel/default") ?>">
                    <notifications_nonespan class="chapter-title"><?= Yii::$app->id ?></notifications_nonespan>
                </a>
            </div>
            <ul class="right col s9 m5 nav-right-menu">
                <!--                <li><a href="javascript:void(0)" data-activates="chat-sidebar" class="chat-button show-on-large"><i class="material-icons">more_vert</i></a></li>-->
                <li class="hide-on-small-and-down">
                    <a href="<?= Url::to("/sign/out") ?>" data-activates="dropdown1">
                        <i class="material-icons">exit_to_app</i>
                    </a>
                </li>
                <!--                Mobile menu items -->
                <li class="hide-on-med-and-up">
                    <a href="<?= Url::to("/sign/out") ?>" title="<?= Yii::t('app', 'Sign out') ?>">
                        <i class="material-icons">exit_to_app</i>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>
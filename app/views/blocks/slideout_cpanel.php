<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 07.07.2019
 * Time: 17:19
 */


use app\models\User;

?>

<aside id="slide-out" class="side-nav white fixed">
    <div class="side-nav-wrapper">
        <div class="sidebar-profile">
            <div class="sidebar-profile-image">
                <img src="<?= (file_exists(Yii::getAlias('@webroot') . "/" . $account->avatar)) ? $account->avatar : "/src/back/assets/images/profile-image.png?>" ?>"
                     class="circle" alt="">
            </div>
            <div class="sidebar-profile-info">
                <a href="javascript:void(0);" class="saccount-settings-link">
                    <p><?= $account->username ?></p>
                    <span>15000</span>
                </a>
            </div>
        </div>
        <? if ($account->is_client != User::USER_REGISTERED_CLIENT) {
            echo \app\widgets\Menu::widget([]);?>
            <li class="no-padding">
                    <a href="<?=\yii\helpers\Url::to(['/cpanel/sale/scan'])?>" class="collapsible-header waves-effect waves-grey"><i
                                class="material-icons">exit_to_app</i><?=Yii::t('app','Scanner')?></a>
        </li>
        <?} else { ?>
            <ul class="sidebar-menu collapsible collapsible-accordion">
                <li class="no-padding">
                    <a href="<?=\yii\helpers\Url::to(['/cpanel/default/index'])?>" class="collapsible-header waves-effect waves-grey"><i
                                class="material-icons">settings_input_svideo</i><?=Yii::t('app','Dashboard')?></a>
                </li>
                <li class="no-padding">
                    <a href="<?=\yii\helpers\Url::to(['/sign/out'])?>" class="collapsible-header waves-effect waves-grey"><i
                                class="material-icons">exit_to_app</i><?=Yii::t('app','Sign out')?></a>
                </li>

                <li class="divider"></li>
                <li class="no-padding">
                    <a class="collapsible-header waves-effect waves-grey"><i
                                class="material-icons">vpn_key</i><?=$account->open_id?></a>
                </li>
                <li class="no-padding">
                    <a class="collapsible-header waves-effect waves-grey"><i
                                class="material-icons">aspect_ratio</i><?=$account->qr?></a>
                </li>

                <li class="no-padding">
                    <a href="<?=\yii\helpers\Url::to('/cpanel/default/scan')?>" class="collapsible-header waves-effect waves-grey"><i
                                class="material-icons">aspect_ratio</i>Scanner</a>
                </li>
            </ul>
        <? } ?>

    </div>
</aside>
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = Yii::t("app", 'Sign up');

?>
<div class="loader-bg"></div>
<div class="loader">
    <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-blue">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
        <div class="spinner-layer spinner-red">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
        <div class="spinner-layer spinner-yellow">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
        <div class="spinner-layer spinner-green">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
    </div>
</div>
<div class="mn-content valign-wrapper">
    <main class="mn-inner container">
        <div class="valign">
            <div class="row">
                <div class="col s12 m6 l4 offset-l4 offset-m3">
                    <div class="card white darken-1">
                        <div class="card-content ">
                            <span class="card-title"><?= Yii::t("app", 'Sign up') ?></span>
                            <div class="row">
                                <?php $form = ActiveForm::begin([
                                    'options' => [
                                        'class' => 'col s12'
                                    ],
                                ]); ?>
                                <div class="input-field col s12">
                                    <?= $form->field($model, 'fullname')
                                        ->textInput([
                                            'autofocus' => true,
                                            'tabindex' => 1
                                        ]); ?>
                                </div>
                                <div class="input-field col s12">
                                    <?= $form->field($model, 'username')
                                        ->textInput([
                                            'tabindex' => 2,
                                            'class' => ' required ',
                                        ]) ?>
                                </div>
                                <div class="input-field col s12">
                                    <?= $form->field($model, 'password')
                                        ->passwordInput([
                                            'tabindex' => 3
                                        ]) ?>
                                </div>
                                <div class="col s12 right-align m-t-sm">

                                    <?= Html::submitButton(Yii::t('app', 'Sign up'), ['class' => 'btn-block waves-effect waves-light btn teal', 'tabindex' => 4]); ?>
                                    <p>
                                        <a href="<?= Url::to('/sign/in') ?>"
                                        ><?= Yii::t('app', 'Login') ?></a>
                                    </p>
                                </div>

                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>

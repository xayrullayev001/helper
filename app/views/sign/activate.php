<?php

/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 26.11.2019
 * Time: 1:31
 */

/* @var $this \yii\web\View */
?>

<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t("app",'Activate account');
?>
<div class="loader-bg"></div>
<div class="loader">
    <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-blue">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
        <div class="spinner-layer spinner-red">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
        <div class="spinner-layer spinner-yellow">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
        <div class="spinner-layer spinner-green">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
    </div>
</div>
<div class="mn-content valign-wrapper">
    <main class="mn-inner container">
        <div class="valign">
            <div class="row">
                <div class="col s12 m6 l4 offset-l4 offset-m3">
                    <div class="card white darken-1">
                        <div class="card-content ">
                            <span class="card-title"><?=Yii::t("app",'Activate account')?></span>
                            <div class="row">
                                <?php $form = ActiveForm::begin([
                                    'options'=>[
                                        'class' => 'col s12',
                                    ],
                                ]); ?>
                                <div class="input-field col s12">
                                    <?= $form->field($model, 'activation_code')->textInput(['autofocus' => true,'id' => 'u_name']) ?>
                                </div>

                                <div class="col s12 right-align m-t-sm">
                                    <?=Html::submitButton(Yii::t('app','Activate'),['class'=>'waves-effect waves-light btn teal']);?>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>


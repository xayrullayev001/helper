<?php

/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 10.01.2020
 * Time: 20:34
 */

/* @var $this \yii\web\View */
/* @var $exception \Exception|null */
?>

<main class="mn-inner">
    <div class="center">
        <h1>
            <span><?=$exception->statusCode?></span>
        </h1>
        <span class="text-white"><?=$exception->getMessage()?></span><br>
        <a class="btn-floating btn-large waves-effect waves-light teal lighten-2 m-t-lg" href="/">
            <i class="large material-icons">home</i>
        </a>
    </div>
</main>


<?php

use app\assets\CPanelAsset;
use yii\helpers\Html;

CPanelAsset::register($this);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="description" content="HELPER"/>
    <meta name="keywords" content="HELPER"/>
    <meta name="author" content="Xayrullayev Behzod"/>

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php
$account = Yii::$app->user->identity;
$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
$this->beginBody();

?>

<div class="mn-content fixed-sidebar">
    <?= Yii::$app->controller->renderPartial("//blocks/header_cpanel",['account'=>$account]); ?>
    <?= Yii::$app->controller->renderPartial("//blocks/searchresult_cpanel"); ?>
    <?= Yii::$app->controller->renderPartial('//blocks/slideout_cpanel', ['account' => $account]) ?>

    <main class="mn-inner ">
        <?= $content ?>
    </main>

    <?= Yii::$app->controller->renderPartial("//blocks/footer_cpanel"); ?>
</div>
<div class="left-sidebar-hover"></div>

<?= $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

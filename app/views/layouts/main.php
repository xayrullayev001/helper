<?php
/**
 * @var $content View
 */
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\web\View;

AppAsset::register($this);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="description" content="HELPER"/>
    <meta name="keywords" content="HELPER"/>
    <meta name="author" content="HELPER"/>

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="body-wrapper">
<?= $this->beginBody();
?>

<div class="main_wrapper">
    <?= Yii::$app->controller->renderPartial("//blocks/header"); ?>

        <?= $content ?>

    <?= Yii::$app->controller->renderPartial("//blocks/footer"); ?>
</div>
<div class="left-sidebar-hover"></div>

<?= $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

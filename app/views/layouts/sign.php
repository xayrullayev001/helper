<?php

use yii\helpers\Html;

use app\assets\SignAsset;

SignAsset::register($this);
/**
 * $content yii\web\View
 */

echo \app\widgets\Toast::widget([
    'options'=>[
        'success'=>'teal',
        'error'=>'deep-orange darken-3',
        'warning'=>'amber accent-4',
        'info'=>'blue'
    ]
]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="description" content="HELPER"/>
    <meta name="keywords" content="HELPER"/>
    <meta name="author" content="Xayrullayev Behzod"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="signin-page">

<?php $this->beginBody() ?>

<?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

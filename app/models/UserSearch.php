<?php

namespace app\models;

use app\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserSearch represents the model behind the search form of `app\models\User`.
 */
class UserSearch extends User
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'status', 'is_client', 'open_id'], 'integer'],
            [['fullname', 'username', 'avatar', 'password', 'authKey', 'accessToken', 'qr'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function getActiveDataProvider($params, $query)
    {
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'fullname', $this->fullname])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'avatar', $this->avatar])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'authKey', $this->authKey])
            ->andFilterWhere(['like', 'accessToken', $this->accessToken]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        return $this->getActiveDataProvider($params, $query);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchByStore($params)
    {
        $query = User::find()
            ->where(['store_id' => Yii::$app->user->identity->store_id])
            ->andWhere(['!=', 'id', Yii::$app->user->identity->getId()]);
        return $this->getActiveDataProvider($params, $query);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchByStoreId($params)
    {
        $storeId = \Yii::$app->user->identity->store_id;
        $query = User::find()->where(['store_id' => $storeId]);
        return $this->getActiveDataProvider($params, $query);

    }

    public function searchByRole(array $params, string $userRole)
    {
        $query = User::find()
            ->leftJoin('auth_assignment', 'user.id = auth_assignment.user_id')
            ->where(['auth_assignment.item_name' => $userRole]);

        return $this->getActiveDataProvider($params, $query);
    }


}

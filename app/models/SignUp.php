<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\httpclient\Client;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $fullname
 * @property int $username
 * @property string $password
 */
class SignUp extends Model
{
    public $password;
    public $fullname;
    public $username;


    public function rules()
    {
        return [
            [['fullname','username', 'password'], 'required'],
            [['fullname','username'], 'string'],
            [['username'], 'unique','targetClass' => User::className(),'message' => 'Username already exists'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fullname' => Yii::t('app', 'Fullname'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
        ];
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function signup($id = false)
    {
        $user = new User();
        $user->username = $this->username;
        $user->fullname = $this->fullname;
        $user->generateAuthKey();
        $user->generateToken();
        $user->status = User::USER_CREATED;
        $user->is_client = User::USER_REGISTERED_CLIENT;
        $user->store_id = User::USER_STORE_NOT;
        $user->open_id = mt_rand(11111,99999);
        $user->qr = "QRCODE"; //TODO qr generatsiya qilib  qoyib ketish kk
        $user->setPassword($this->password);
        Registrators::createRow($id,$user->username);
        return $user->validate() && $user->save();
    }

    public function sendActivationCode($username,$message){
       
    }


    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setTextBody($this->body)
                ->send();

            return true;
        }
        return false;
    }

}

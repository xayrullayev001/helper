<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "menus".
 *
 * @property int $id
 * @property string $slug
 * @property string $label
 * @property string $url
 * @property integer $parent_id
 * @property integer $sort_order
 * @property integer $has_child
 * @property integer $status
 * @property string $class
 * @property string $icon
 * @property string $link_template
 * @property string $permission default value NOT_CHECK
 * @property integer $created_at
 * @property integer $updated_at
 */
class Menus extends ActiveRecord
{
    const PUBLIC = 'PUBLIC';
    const MENU_PARENT_NOT = 0;
    public $controller = "#";
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menus';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'slugAttribute' => 'slug',
                'attribute' => 'label'
            ],
            [
                'class'=>TimestampBehavior::className()
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['slug', 'label'], 'required'],
            [['id', 'sort_order','status','has_child'], 'integer'],
            [['parent_id','permission'], 'safe'],
            [['icon','controller'], 'string','max'=>50],
            [['slug', 'label', 'url','link_template','class'], 'string', 'max' => 512],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'slug' => Yii::t('app', 'Slug'),
            'label' => Yii::t('app', 'Label'),
            'url' => Yii::t('app', 'Url'),
            'parent_id' => Yii::t('app', 'Parent'),
            'sort_order' => Yii::t('app', 'Order'),
            'class' => Yii::t('app', 'Class'),
            'icon' => Yii::t('app', 'Icon'),
            'link_template' => Yii::t('app', 'Link template'),
            'permission' => Yii::t('app', 'Permission'),
            'controller' => Yii::t('app', 'Controller'),
        ];
    }

    public function getParent(){
        return $this->hasOne(Menus::className(),['id'=>'parent_id']);
    }

    public function getWithChilds(){
        return $this->hasMany(Menus::className(),['parent_id'=>'id'])->with('withChilds');
    }

}

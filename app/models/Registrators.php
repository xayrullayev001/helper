<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "registrators".
 *
 * @property int $id
 * @property string $auth_key
 * @property int $store_id
 * @property int $created_at
 * @property int $updated_at
 */
class Registrators extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'registrators';
    }
    public function behaviors()
    {
        return [['class'=>TimestampBehavior::className()]];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['auth_key', 'store_id', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['store_id', 'auth_key'], 'string', 'max' => 512],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'store_id' => Yii::t('app', 'Store ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public static function createRow($authKey,$storeId){
        $row = new Registrators();
        $row->auth_key = $authKey;
        $row->store_id = $storeId;
        $row->created_at = $row->updated_at = time();
         $row->save();
    }
}

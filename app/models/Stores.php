<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "stores".
 *
 * @property int $id
 * @property int $updated_at
 * @property int $created_at
 * @property string $name
 * @property string $logo
 * @property string $role
 * @property int $status
 * @property int $cashback
 * @property int $average_check
 * @property int $category
 * @property int $manager
 */
class Stores extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className()
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'logo', 'role'], 'required'],
            [['updated_at', 'created_at', 'status','cashback','category','manager'], 'integer'],
            [['name', 'logo', 'role'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
            'name' => Yii::t('app', 'Name'),
            'logo' => Yii::t('app', 'Logo'),
            'role' => Yii::t('app', 'Role'),
            'status' => Yii::t('app', 'Status'),
            'cashback' => Yii::t('app', 'CashBack'),
            'average_check' => Yii::t('app', 'Average Check'),
            'category' => Yii::t('app', 'Store category'),
            'manager' => Yii::t('app', 'Manager'),
        ];
    }
    public function getCat()
    {
        return $this->hasOne(StoreCategory::className(), ['id' => 'category']);
    }
    public function getStoreManager()
    {
        return $this->hasOne(ManagerStores::className(), ['manager_id' => 'id']);
    }
}

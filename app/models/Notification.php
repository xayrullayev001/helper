<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "notification".
 *
 * @property integer $id
 * @property integer $from
 * @property integer $to
 * @property string $message
 * @property integer $message_order
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Notification extends \yii\db\ActiveRecord
{

    const STATUS_VIEWED = 1;
    const STATUS_CREATED = 0;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification';
    }

    public static function forUser($getId)
    {
        return self::findAll(['to'=>$getId,'status'=>false]);
    }

     /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from', 'to', 'message', 'message_order', 'created_at', 'updated_at'], 'required'],
            [['from', 'to', 'message_order', 'created_at', 'updated_at','status'], 'integer'],
            [['message'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'from' => Yii::t('app', 'From'),
            'to' => Yii::t('app', 'To'),
            'message' => Yii::t('app', 'Message'),
            'message_order' => Yii::t('app', 'Message Order'),
            'status' => Yii::t('app', 'Message status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    public static function create($from,$to,$message,$order=1, $status=0)
    {
        $model = new Notification();
        $model->from = $from;
        $model->to = $to;
        $model->message = $message;
        $model->order = $order;
        $model->status = $status;
        return $model->save();
    }
}

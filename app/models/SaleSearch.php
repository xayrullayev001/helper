<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Sale;

/**
 * SoldSearch represents the model behind the search form of `app\models\Sold`.
 */
class SaleSearch extends Sale
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'salesman', 'updated_at', 'created_at', 'store_id'], 'integer'],
            [['product_id'], 'safe'],
            [['summa'], 'number'],
            [['qr_code'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sale::find()->where(['store_id'=>\Yii::$app->user->identity->store_id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'salesman' => $this->salesman,
            'summa' => $this->summa,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
            'store_id' => $this->store_id,
        ]);

        $query->andFilterWhere(['like', 'product_id', $this->product_id]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchByCompany($params)
    {
        $query = Sale::find()->where(['store_id'=>\Yii::$app->user->identity->store_id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'salesman' => $this->salesman,
            'summa' => $this->summa,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
            'store_id' => $this->store_id,
        ]);

        $query->andFilterWhere(['like', 'product_id', $this->product_id]);

        return $dataProvider;
    }
}

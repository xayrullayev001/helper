<?php

namespace app\models;

use app\components\QRBehavior;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $updated_at
 * @property int $created_at
 * @property string $name
 * @property double $price
 * @property int $discount
 * @property int $store_id
 * @property string $image
 * @property double $count
 * @property string $comment
 * @property string $type
 */
class Products extends \yii\db\ActiveRecord
{
    public static function findProducts()
    {
        return self::find()
            ->where(['store_id' => Yii::$app->user->identity->store_id])
            ->asArray()
            ->all();
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className()
            ],
            [
                'class' => QRBehavior::className(),
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'store_id',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'store_id',
                ],
                'value' => function ($event) {
                    return Yii::$app->user->identity->store_id;
                },
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'price', 'discount', 'count'], 'required'],
            [['updated_at', 'created_at', 'discount', 'store_id','type'], 'integer'],
            [['price', 'count'], 'number'],
            [['name', 'image', 'comment'], 'string', 'max' => 1024],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
            'name' => Yii::t('app', 'Name'),
            'price' => Yii::t('app', 'Price'),
            'discount' => Yii::t('app', 'Discount'),
            'image' => Yii::t('app', 'Image'),
            'count' => Yii::t('app', 'Count'),
            'comment' => Yii::t('app', 'Comment'),
            'store_id' => Yii::t('app', 'Store'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    public function getTyp()
    {
        return $this->hasOne(ProductTypes::className(), ['id' => 'type']);
    }

    public function getStore()
    {
        return $this->hasOne(Stores::className(), ['id' => 'store_id']);
    }
}

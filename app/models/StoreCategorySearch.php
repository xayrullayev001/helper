<?php

namespace app\models;

use app\models\StoreCategory;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * StoreCategorySearch represents the model behind the search form of `app\models\StoreCategory`.
 */
class StoreCategorySearch extends StoreCategory
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function getActiveDataProvider($params, $query)
    {
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreCategory::find();
        return $this->getActiveDataProvider($params,$query);
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchWithStores($params)
    {
        $query = StoreCategory::find()
            ->with('stores');
        return $this->getActiveDataProvider($params,$query);
    }

    public function searchManagerStores(array $params,$managerId)
    {
        $query = Stores::find()
            ->with('cat')
        ->where(['manager'=>$managerId]);

        return $this->getActiveDataProvider($params,$query);
    }
}

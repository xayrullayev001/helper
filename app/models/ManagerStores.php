<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "manager_stores".
 *
 * @property int $id
 * @property int $manager_id
 * @property int $store_id
 * @property int $updated_at
 * @property int $created_at
 */
class ManagerStores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'manager_stores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['manager_id', 'store_id', 'updated_at', 'created_at'], 'required'],
            [['manager_id', 'store_id', 'updated_at', 'created_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'manager_id' => Yii::t('app', 'Manager ID'),
            'store_id' => Yii::t('app', 'Store ID'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
}

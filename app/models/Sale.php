<?php

namespace app\models;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sold".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $salesman
 * @property string $product_id
 * @property double $summa
 * @property string $qr_code
 * @property int $updated_at
 * @property int $created_at
 * @property int $store_id
 */
class Sale extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sold';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className()
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_VALIDATE => ['salesman', 'customer_id']
                ],
                'value' => Yii::$app->user->identity->getId()
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['store_id']
                ],
                'value' => Yii::$app->user->identity->store_id
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['qr_code']
                ],
                'value' => function(){
                    return "salom qr code";
                }
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'salesman', 'summa', 'store_id'], 'required'],
            [['customer_id', 'salesman', 'updated_at', 'created_at', 'store_id'], 'integer'],
            [['summa'], 'number'],
            [['qr_code'], 'string'],
            [['product_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'salesman' => Yii::t('app', 'Salesman'),
            'product_id' => Yii::t('app', 'Product ID'),
            'summa' => Yii::t('app', 'Summa'),
            'qr_code' => Yii::t('app', 'Qr Code'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
            'store_id' => Yii::t('app', 'Store ID'),
        ];
    }

    public function getSalesMan()
    {
        return $this->hasOne(User::className(), ['id' => 'salesman']);
    }
}

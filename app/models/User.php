<?php

namespace app\models;

use app\components\qr\ModeratorAuthKeyFormatter;
use Da\QrCode\QrCode;
use Yii;
use yii\base\Exception;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $fullname
 * @property int $username
 * @property int $activation_code
 * @property string $password
 * @property string $avatar
 * @property int $created
 * @property int $status
 * @property string $authKey
 * @property string $accessToken
 * @property string $qr_code
 * @property int $store_id
 * @property int $created_at
 * @property int $updated_at
 * @property int|mixed|null $is_client
 * @property int|mixed|null $open_id
 * @property int|mixed|null $qr
 */
class User extends ActiveRecord implements IdentityInterface
{

    /**
     * users new created status
     */
    const USER_CREATED = 0;
    /**
     *  users when activated
     */
    const USER_ACTIVATED = 1;
    /**
     * users when admin confirmed
     */
    const USER_CONFIRMED = 2;
    /**
     * users when blocked
     */
    const USER_BLOCKED = 3;
    /**
     * for root user
     */
    const USER_ROOT = 5;
    const USER_REGISTERED_CLIENT = 1;
    const USER_STORE_NOT = 0;
    /**
     * @var bool
     */
    public $remberme = true;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className()
            ]
        ];
    }

    /**
     * @return array
     */
    public static function dropDownList()
    {
        return ArrayHelper::map(self::findAll(['status' => User::USER_CREATED]), 'id', 'fullname');
    }
    public function setQrCode(){
        $format = new ModeratorAuthKeyFormatter(['url' => Yii::$app->urlManager->createAbsoluteUrl(["/sign/up",'id'=>$this->authKey])]);
        $qrCode = new QrCode($format);
        $name = Yii::$app->security->generateRandomString(12);
        $path = Yii::getAlias('@webroot')."/uploads/authkeys/{$name}.png";
        $qrCode->writeFile($path);
        $this->qr_code = $path;
        return $this->qr_code;
    }
    /**
     * @return int|string
     */
    public static function count($status = self::USER_CREATED)
    {
        return self::find()->where(['status' => $status])->count();
    }

    /**
     * @return User[]|array|ActiveRecord[]
     */
    public static function getNewAccounts()
    {
        return self::find()->where(['status' => self::USER_CREATED])->all();
    }

    /**
     * @param int|string $id
     * @return User|null|IdentityInterface
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @param mixed $token
     * @param null $type
     * @return void|IdentityInterface
     * @throws NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function usersByRole($roleName)
    {
        $authManager = Yii::$app->getAuthManager();
        return $authManager->getUserIdsByRole($roleName);
    }

    public static function canUpdate($owner)
    {
        return ($owner === Yii::$app->user->identity->id || User::hasRole('ADMINISTRATOR', Yii::$app->user->identity->id)) ? true : false;
    }

    public static function hasRole($roleName, $userId)
    {
        $authManager = Yii::$app->getAuthManager();
        return $authManager->getAssignment($roleName, $userId) ? true : false;
    }

    public function getStatus()
    {
        $status = [
            self::USER_CREATED => Yii::t('app', 'Created'),
            self::USER_ACTIVATED => Yii::t('app', 'Activated'),
            self::USER_BLOCKED => Yii::t('app', 'Blocked'),
            self::USER_CONFIRMED => Yii::t('app', 'Confirmed'),
        ];
        return $status[$this->status];

    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['fullname', 'username', 'password'], 'required'],
            [['created_at','updated_at', 'status', 'store_id','is_client','open_id'], 'integer'],
            [['created_at', 'updated_at', 'status', 'store_id'], 'integer'],
            [['username', 'fullname'], 'string', 'max' => 50],
            [['qr_code'], 'string', 'max' => 128],
            [['authKey', 'accessToken', 'avatar'], 'string', 'max' => 512],
            [['password','qr_code'], 'string', 'max' => 1024],

        ];
    }


    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fullname' => Yii::t('app', 'Fullname'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'created_at' => Yii::t('app', 'Created'),
            'updated_at' => Yii::t('app', 'Updated'),
            'status' => Yii::t('app', 'Status'),
            'authKey' => Yii::t('app', 'Auth Key'),
            'accessToken' => Yii::t('app', 'Access Token'),
            'avatar' => Yii::t('app', 'Avatar'),
            'store_id' => Yii::t('app', 'Store'),
            'is_client' => Yii::t('app', 'Client'),
            'open_id' => Yii::t('app', 'Open id'),
            'qr_code' => Yii::t('app', 'QR code'),
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        $sc = parent::scenarios();
        $sc[self::USER_CREATED] = ['fullname', 'username', 'password'];
        $sc[self::USER_BLOCKED] = ['status'];
        return $sc;
    }

    /**
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User|null|string
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @param string $authKey
     * @return User|bool|null
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey == $authKey;
    }

    /**
     * @param $password
     * @return bool
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    /**
     * @throws Exception
     */
    public function generateAuthKey()
    {
        $this->authKey = Yii::$app->security->generateRandomString();
    }

    /**
     * @return string
     * @throws Exception
     */
    public function generateToken()
    {
        return $this->accessToken = Yii::$app->security->generateRandomString();
    }

    /**
     * @param $password
     * @throws Exception
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

}


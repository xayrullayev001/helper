<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Notification */

$this->title = Yii::t('app', 'Update Notification: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Notifications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="row">
    <div class="col s12 m12 l12">
        <div class="row">
            <div class="card">
                <div class="card-content">
                    <div class="row" style="margin-bottom: 0px;">
                        <div class="page-title">
                            <?= Html::encode($this->title) ?>
                        </div>
                        <div class="page-title-buttons">
                            <a class="btn btn-primary" href="/gii">Orqaga</a>                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <?= $this->render('_form', [
                'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>

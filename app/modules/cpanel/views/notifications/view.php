<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Notification */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Notifications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="row">
    <div class="col s12 m12 l12">
        <div class="row">
            <div class="card">
                <div class="card-content">
                    <div class="row" style="margin-bottom: 0px;">
                        <div class="page-title">
                                                    </div>
                        <div class="page-title-buttons">
                            <?= Html::a(Yii::t('app', 'Update'),
                            ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a(Yii::t('app', 'Delete'),
                            ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?')                            ,
                            'method' => 'post',
                            ],
                            ]) ?>
                            <?=                             Html::a(Yii::t('app', 'Create Notification')                            , ['create'], ['class' => 'btn btn-success']) ?>
                            <a class="btn btn-primary" href="/gii">Orqaga</a>                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">


                <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                            'id',
            'from',
            'to',
            'message',
            'message_order',
            'created_at',
            'updated_at',
            'status',
                ],
                ]) ?>

            </div>
        </div>
    </div>
</div>


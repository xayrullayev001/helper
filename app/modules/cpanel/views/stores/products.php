<?php

/**
 * @var $store \app\models\Stores
 * @var $dataProvider \yii\data\ActiveDataProvider
 *
 */

use yii\helpers\Url;

?>

<!-- Main view -->
<div class="view">
    <!-- Blueprint header -->
    <header class="bp-header cf">
        <span><?= $store->name ?> <span class="bp-icon bp-icon-about" data-content="<?= $store->name ." average check: ".$store->average_check?>"></span></span>
        <nav>
            <a href="<?= Url::to(['/cpanel/default/index']) ?>" class="bp-icon bp-icon-prev"><span>All stores</span></a>
        </nav>
    </header>
    <!-- Grid -->
    <section class="grid grid--loading">
        <!-- Loader -->
        <img class="grid__loader" src="/web/plugins/ggrid/images/grid.svg" width="60" alt="Loader image"/>
        <!-- Grid sizer for a fluid Isotope (Masonry) layout -->
        <div class="grid__sizer"></div>
        <!-- Grid items -->
        <? foreach ($dataProvider->models as $type) { ?>


            <!-- Bottom bar with filter and cart info -->
            <div class="bar">
                <div class="filter">
                    <span class="filter__label">Filter: </span>
                    <button class="action filter__item filter__item--selected" data-filter="*">All</button>
                    <button class="action filter__item" data-filter=".<?= $type->slug ?>">
                        <i class="<?= $type->icon ?>"></i>
                        <span class="action__text"><?= $type->name ?></span></button>
                </div>
                <button class="cart">
                    <i class="cart__icon fa fa-shopping-cart"></i>
                    <span class="text-hidden">Shopping cart</span>
                    <span class="cart__count">0</span>
                </button>
            </div>

            <? foreach ($type->products as $product) { ?>
                <?$size = ($product->id % 7 == 0)?"grid__item--size-a":''?>
                <div class="grid__item <?= $type->slug . " ".$size?>">
                    <div class="slider">
                        <div class="slider__item"><img src="<?= $product->image ?>" alt="Dummy"/></div>
                    </div>
                    <div class="meta">
                        <h3 class="meta__title"><?= $product->name ?></h3>
                        <span class="meta__brand">Dummy Brand</span>
                        <span class="meta__price"><?= $product->price ?></span>
                    </div>
                    <button class="action action--button action--buy"><i class="fa fa-shopping-cart"></i><span
                                class="text-hidden">Add to cart</span></button>
                </div>
            <? } ?>

        <? } ?>

    </section>
    <!-- /grid-->
</div>
<!-- /view -->
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->fullname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="row" style="margin-bottom: 0px;">
                    <div class="page-title">
                        <?= Html::encode($this->title) ?>
                    </div>
                    <div class="page-title-buttons">
                        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ]) ?>
                        <?= Html::a(Yii::t('app', 'Back'), ['index'], ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'fullname',
                        'username',
                        'avatar:image',
                        'password',
                        'status',
                        'updated_at:date',
                        'created_at:date',
                        [
                            'label' => 'status',
                            'value' => function ($model) {
                                return $model->getStatus();
                            }
                        ],
                        [
                            'label' => Yii::t('app', 'Role'),
                            'value' => function ($model) {
                                $roles = Yii::$app->authManager->getRolesByUser($model->id);
                                return $roles[array_key_first($roles)]->description;
                            },
                            'format' => 'text'
                        ],
//                        'status',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>

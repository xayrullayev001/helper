<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::t('app', 'Update manager: {name}', [
    'name' => $model->username,
]);
?>
<div class="row">
    <div class="col s12 m12 l12">
        <div class="row">
            <div class="card">
                <div class="card-content">
                    <div class="row" style="margin-bottom: 0px;">
                        <div class="page-title">
                            <?= Html::encode($this->title) ?>
                        </div>
                        <div class="page-title-buttons">
                            <?= Html::a(Yii::t('app', 'Back'), ['managers'], ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <div class="user-form">

                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'fullname')->textInput(['maxlength' => true,'autofocus'=>true,'tabindex'=>1]) ?>

                    <?= $form->field($model, 'username')->textInput([
                        'tabindex' => 2,
                        'class'=>'required',
                    ]) ?>

                    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true,'tabindex' => 3]) ?>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success','tabindex' => 5]) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>

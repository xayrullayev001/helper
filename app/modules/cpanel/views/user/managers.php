<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Managers');
?>
<div class="row">
    <div class="col s12 m12 l12">
        <div class="row">
            <div class="card">
                <div class="card-content">
                    <div class="row" style="margin-bottom: 0px;">
                        <div class="page-title">
                            <?= Html::encode($this->title) ?>
                        </div>
                        <div class="page-title-buttons">
                            <?= Html::a(Yii::t('app', 'Add manager'), ['create-manager'], ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">


                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'username',
                        ],
                        [
                            'attribute' => 'fullname',
                            'headerOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                            'contentOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                            'value' => function ($model) {
                                return $model->fullname;
                            }

                        ],
                        [
                            'attribute' => 'created_at',
                            'headerOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                            'contentOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                            'format' => 'date'
                        ],
                        [
                            'attribute' => 'status',
                            'headerOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                            'contentOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                            'value' => function ($model) {
                                return $model->status;
                            }
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view} {update} {delete}',
                            'header' => Yii::t('app', 'Actions'),
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    $url = Url::to(['/cpanel/user/view-manager','id'=>$model->id]);
                                    return Html::a(
                                        '<span class="material-icons" title="' . Yii::t('app', 'View user data') . '">remove_red_eye</span>', $url);
                                },
                                'update' => function ($url, $model) {
                                    $url = Url::to(['/cpanel/user/update-manager','id'=>$model->id]);
                                    return Html::a(
                                        '<span class="material-icons" title="' . Yii::t('app', 'Update user data') . '">mode_edit</span>', $url);
                                },
                                'delete' => function ($url, $model) {
                                    $url = Url::to(['/cpanel/user/delete-manager','id'=>$model->id]);
                                    return Html::a(
                                        '<span class="material-icons" title="' . Yii::t('app', 'Delete user data') . '">delete</span>', $url, [
                                        'data' => [
                                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                            'method' => 'post',
                                        ],
                                    ]);
                                },
                            ]
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fullname')->textInput(['maxlength' => true,'autofocus'=>true,'tabindex'=>1]) ?>

    <?= $form->field($model, 'username')->textInput([
                                            'tabindex' => 2,
                                            'class'=>'required',
                                        ]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true,'tabindex' => 3]) ?>
    <?php
    if(Yii::$app->user->can("Store-employee-add")){
        echo $form->field($model, 'store_id')->hiddenInput(['value'=>Yii::$app->user->identity->store_id])->label(false);
    }else{
        echo $form->field($model, 'store_id')->dropDownList(
            \yii\helpers\ArrayHelper::map(\app\models\Stores::find()->all(), 'id', 'name')
        );
    }
    ?>

    <?= $form->field($model, 'avatar', ['template' =>
        '<div class="file-field input-field">
        <div class="btn teal lighten-1">
            <span>' . $model->getAttributeLabel('avatar') . '</span>
            {input}
        </div>
        <div class="file-path-wrapper">
            <input class="file-path validate valid" type="text">
        </div>
    </div>{error}'])->fileInput(['maxlength' => true,'tabindex' => 4]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success','tabindex' => 5]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

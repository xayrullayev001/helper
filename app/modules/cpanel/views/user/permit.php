<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\components\auth\models\AuthItem;
use yii\rbac\Permission;

/* @var $this yii\web\View */
/* @var $model app\components\auth\models\AuthAssignment */
/* @var $form ActiveForm */
/* @var $roles \yii\rbac\Role */
/* @var $users \app\models\User */
$this->title = Yii::t('app', 'Permit user');
?>
<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="row" style="margin-bottom: 0px;">
                    <div class="page-title">
                        <?= Html::encode($this->title) ?>
                    </div>
                    <div class="page-title-buttons">
                        <?= Html::a(Yii::t('app', 'Back'), ['index'], ['class' => 'btn btn-success ']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <div class="form-_form">
                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'item_name')->dropDownList(
                        ArrayHelper::map($roles, 'name', 'name')
                    ) ?>
                    <?= $form->field($model, 'user_id')->hiddenInput(['value' => $user])->label(false) ?>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div><!-- form-_form -->
            </div>
        </div>
    </div>
</div>


<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 06.10.2019
 * Time: 23:20
 */

use yii\rbac\Permission;
use yii\rbac\Item;
use yii\rbac\Role;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;


$this->title = Yii::t('app', 'Create role');
?>
<div class="debitor-view">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <?= \yii\bootstrap\Html::encode($this->title) ?>
            </div>
        </div>
    </div>
    <?php
    echo Html::a(Yii::t("app", "Create"), ['/auth/item-child/create'], ['class' => 'btn btn-primary']);
    echo Html::a(Yii::t('app', 'Back'), ['index'], ['class' => 'btn btn-primary']);
    ?>
    <div class="col s12 m12 l6">
        <div class="card">
            <div class="card-content">
                <?php
                echo
                \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => \yii\grid\SerialColumn::className()],
                        ['label' => 'Name',
                            'value' => function ($model) {
                                return $model['name'];
                            },
                        ],
                        [
                            'label' => 'Type',
                            'value' => function ($model) {
                                return $model['type'] == Item::TYPE_PERMISSION ? Permission::className() : Role::className();
                            },
                        ],
                        [
                            'label' => 'description',
                            'value' => function ($model) {
                                return $model['description'];
                            },
                        ],
                        [
                            'label' => 'ruleName',
                            'value' => function ($model) {
                                return $model['rule_name'];
                            },
                        ],
                        [
                            'label' => 'data',
                            'value' => function ($model) {
                                return $data;
                            },
                        ],
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{delete}',
                            'buttons' => [
                                'delete' => function ($url, $model, $key)  {
                                    $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-trash"]);
                                    return \yii\helpers\Html::a($icon, Url::to(['role/delete',  'child' => $model['name']]));
                                }
                            ],
                        ],

                    ]
                ]);
                ?>

            </div>
        </div>
    </div>

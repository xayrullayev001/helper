<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 23:41
 */

use yii\rbac\Permission;
use yii\rbac\Item;
use yii\rbac\Role;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app','Permissions');
?>
<div class="col s12">
    <div class="card">
        <div class="card-content">
            <?= \yii\bootstrap\Html::encode($this->title) ?>
        </div>
    </div>
</div>

<?php
echo Html::a(Yii::t("app", "Create"), ['/auth/permission/create'], ['class' => 'btn btn-primary']);
echo Html::a(Yii::t('app', 'Back'), ['/auth'], ['class' => 'btn btn-primary']);

?>
<div class="col s12 m12 l6">
    <div class="card">
        <div class="card-content">

            <?php
            echo
            \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => \yii\grid\SerialColumn::className()],
                    ['label' => 'Name',
                        'value' => function ($model) {
                            return $model['name'];
                        },
                    ],

                    [
                        'label' => 'description',
                        'value' => function ($model) {
                            return $model['description'];
                        },
                    ],


                    [
                        'class' => ActionColumn::className(),
                        'template' => '{delete}',
                        'buttons' => [
                            'delete' => function ($url, $model, $key)  {
                                $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-trash"]);
                                return \yii\helpers\Html::a($icon, Url::to(['permission/delete', 'name' => $model['name']]));
                            }
                        ],
                    ],

                ]
            ]);
            ?>
        </div>
    </div>
</div>


<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $models \yii\gii\model[] */
/* @var $content string */

$models = Yii::$app->controller->module->models;
$this->title = Yii::t('app', 'Welcome to Auth');

?>

<div class="col s12">
    <div class="card">
        <div class="card-content">
            <h5><?=Yii::t('app', 'Welcome to Auth')?>
                <small> <?=Yii::t('app','for all users roles and authorities');?></small>
            </h5>
            <p class="lead"><?=Yii::t('app','Start the fun with the following modules:');?></p>
        </div>
    </div>
</div>


<div class="row">
    <?php foreach ($models as $id => $model):
        if ($model->getName() == 'Auth rule') continue;
         if (!Yii::$app->user->can("Permission-index") && $model->getName() == 'Item Permission') continue;
         if (!Yii::$app->user->can("Role-index") && $model->getName() == 'Item Role') continue;
        ?>
        <div class="col s12 m4 l4">
            <div class="card">
                <div class="card-content">
                    <h5><?= Html::encode($model->getName()) ?></h5>
                    <p><?= Yii::t('app',$model->getDescription() );?></p>
                    <br>
                    <p><?= Html::a(Yii::t('app','Start').' &raquo;', [$id . '/index'], ['class' => 'btn btn-default']) ?></p>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use vendor\auth\models\AuthItem;
use yii\rbac\Permission;
use app\models\User;
/* @var $this yii\web\View */
/* @var $model vendor\auth\models\AuthAssignment */
/* @var $form ActiveForm */
$userModel = Yii::$app->user->identityClass;
$auth = AuthItem::find()->where(['type' => Permission::TYPE_ROLE])->andWhere(['!=','name','ROOT'])->all();
?>
<div class="form-_form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'item_name')->dropDownList([
        ArrayHelper::map($auth, 'name', 'name')
    ]) ?>
    <?= $form->field($model, 'user_id')->dropDownList([
        ArrayHelper::map(User::findAll(['status'=>User::USER_CONFIRMED]), "id", 'fullname')
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- form-_form -->

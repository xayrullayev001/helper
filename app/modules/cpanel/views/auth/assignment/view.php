<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 06.10.2019
 * Time: 23:19
 */
?>


<div class="col s12 m12 l6">
    <div class="card">
        <div class="card-content">
            <?php
            echo \yii\widgets\DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'parent',
                    'child'
                ]
            ]);

            ?>
        </div>
    </div>
</div>


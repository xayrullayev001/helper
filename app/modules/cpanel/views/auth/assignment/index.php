<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 23:41
 */

use yii\helpers\Html;
$this->title = Yii::t('app','Roles');
?>

<div class="col s12">
    <div class="card">
        <div class="card-content">
            <?= \yii\bootstrap\Html::encode($this->title) ?>
        </div>
    </div>
</div>
<?php
echo Html::a(Yii::t("app", "Create"), ['create'], ['class' => 'btn btn-primary']);
echo Html::a(Yii::t('app', 'Back'), ['/auth'], ['class' => 'btn btn-primary']);

?>

<div class="col s12 m12 l6">
    <div class="card">
        <div class="card-content">
            <?php
            echo
            \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    'item_name',
                     
                    [
                        'attribute'=>'user_id',
                        'value'=>function($model){
                            return $model->username->fullname;
                        }
                    ],
                    
                    ['class' => \yii\grid\ActionColumn::className(),
                        'header' => 'Actions',
                        'template' => '{view} {update}'
                    ]
                ]
            ]);
            ?>

        </div>
    </div>
</div>

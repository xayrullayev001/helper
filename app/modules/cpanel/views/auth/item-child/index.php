<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 23:41
 */

use yii\helpers\Html;

$this->title = Yii::t('app', 'Item child');

?>

<div class="col s12">
    <div class="card">
        <div class="card-content">
            <?= \yii\bootstrap\Html::encode($this->title) ?>
        </div>
    </div>
</div>
<?php
echo Html::a(Yii::t("app", "Create"), ['create'], ['class' => 'btn btn-primary']);
echo Html::a(Yii::t('app', 'Back'), ['/auth'], ['class' => 'btn btn-primary']);
?>
<div class="col s12 m12 l6">
    <div class="card">
        <div class="card-content">

            <?php

            echo
            \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => \yii\grid\SerialColumn::className()],
                    [
                        'label' => 'Parent',
                        'format' => 'html',
                        'value' => function ($model) {
                            return Html::a($model['parent'], ['/auth/role/view', 'name' => $model['parent']]);
                        },
                    ],
                    'child',
                    ['class' => \yii\grid\ActionColumn::className(),
                        'template' => '{view} {update} {delete}',
                    ]
                ]
            ]);
            ?>
        </div>
    </div>
</div>


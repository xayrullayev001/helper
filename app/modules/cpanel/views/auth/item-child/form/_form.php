<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vendor\auth\models\AuthItem;
use yii\helpers\ArrayHelper;
use yii\rbac\Permission;
/* @var $this yii\web\View */
/* @var $model vendor\auth\models\AuthItemChild */
/* @var $form ActiveForm */
$role = AuthItem::find()->where(['type'=>Permission::TYPE_ROLE])->andWhere(['!=','name','ROOT'])->all();
$pms = AuthItem::find()->where(['type'=>Permission::TYPE_PERMISSION])->andWhere(['!=','name','ROOT'])->all();
?>
<div class="form-_form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent')->dropDownList([ ArrayHelper::map($role,'name','name')]) ?>

    <?= $form->field($model, 'child')->dropDownList([ ArrayHelper::map($pms,'name','name')])  ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- form-_form -->

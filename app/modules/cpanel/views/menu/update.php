<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Menus */
/* @var $menus app\models\Menus */

$this->title = Yii::t('app', 'Update Menus: {name}', [
    'name' => $model->label,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menus'), 'url' => ['index']];
?>

<div class="row">
    <div class="col s12 m12 l12">
        <div class="row">
            <div class="card">
                <div class="card-content">
                    <div class="row" style="margin-bottom: 0px;">
                        <div class="page-title">
                            <?= Html::encode($this->title) ?>
                        </div>
                        <div class="page-title-buttons">
                            <?= Html::a(Yii::t('app', 'Back'), ['index'], ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <?= $this->render('_form', [
                    'model' => $model,
                    'menus' => $menus
                ]) ?>
            </div>
        </div>
    </div>
</div>

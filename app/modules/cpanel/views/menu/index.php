<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MenusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $menuItems app\models\Menus */

$this->title = Yii::t('app', 'Menus');
?>

<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="row" style="margin-bottom: 0px;">
                    <div class="page-title">
                        <?= Html::encode($this->title) ?>
                    </div>
                    <div class="page-title-buttons">
                        <?= Html::a(Yii::t('app', 'Create Menu'), ['create'], ['class' => 'btn btn-success ']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <?php Pjax::begin(); ?>
                <ul class="collapsible popout" data-collapsible="accordion">
                    <? foreach ($menuItems as $menu) { ?>
                        <li>
                            <div class="collapsible-header">
                                <i class="material-icons large">
                                    <?= $menu->icon ?>
                                </i>
                                <?= $menu->label ?>
                            </div>
                            <div class="collapsible-body">
                                <p>
                                <div class="container">
                                    <div class="row">
                                        <div class="s12 m6 l3">
                                            <ul>
                                                <li><?= Yii::t('app', 'Url: ') . $menu->url ?></li>
                                                <li><?= Yii::t('app', 'Class: ') . $menu->class ?></li>
                                                <li><?= Yii::t('app', 'Link template: ') . Html::encode($menu->link_template) ?></li>
                                            </ul>
                                        </div>
                                        <div class="s12 m6 l3">
                                            <div class="">
                                                <a href="<?= Url::to(["view", 'id' => $menu->id]) ?>"
                                                   class="waves-effect waves-light btn"><i class="material-icons large">remove_red_eye</i>
                                                </a>
                                                <a href="<?= Url::to(["update", 'id' => $menu->id]) ?>"
                                                   class="waves-effect waves-light btn"><i class="material-icons large">mode_edit</i>
                                                </a>
                                                <a href="<?= Url::to(["delete", 'id' => $menu->id]) ?>"
                                                   class="waves-effect waves-light btn" data-method="post"
                                                   data-confirm="<?= Yii::t('app', 'Are you sure you want to delete this menu?'); ?>">
                                                    <i class="material-icons">delete</i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </p>
                                <? if ($menu->has_child) { ?>
                                    <ul class="collapsible popout" data-collapsible="accordion">
                                        <? foreach ($menu->withChilds as $child) { ?>
                                            <li>
                                                <div class="collapsible-header">
                                                    <i class="material-icons large">
                                                        <?= $child->icon ?>
                                                    </i>
                                                    <?= $child->label ?>
                                                </div>
                                                <div class="collapsible-body"><p>
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="s12 m6 l3">
                                                                <ul>
                                                                    <li><?= Yii::t('app', 'Url: ') . $child->url ?></li>
                                                                    <li><?= Yii::t('app', 'Class: ') . $child->class ?></li>
                                                                    <li><?= Yii::t('app', 'Link template: ') . Html::encode($child->link_template) ?></li>
                                                                </ul>
                                                            </div>
                                                            <div class="s12 m6 l3">
                                                                <div class="">
                                                                    <a href="<?= Url::to(["view", 'id' => $child->id]) ?>"
                                                                       class="waves-effect waves-light btn"><i
                                                                                class="material-icons large">remove_red_eye</i>
                                                                    </a>
                                                                    <a href="<?= Url::to(["update", 'id' => $child->id]) ?>"
                                                                       class="waves-effect waves-light btn"><i
                                                                                class="material-icons large">mode_edit</i>
                                                                    </a>
                                                                    <a href="<?= Url::to(["delete", 'id' => $child->id]) ?>"
                                                                       class="waves-effect waves-light btn"
                                                                       data-method="post"
                                                                       data-confirm="<?= Yii::t('app', 'Are you sure you want to delete this menu?'); ?>">
                                                                        <i class="material-icons">delete</i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </p>
                                                </div>
                                            </li>
                                        <? } ?>
                                    </ul>
                                <? } ?>
                            </div>
                        </li>
                    <? } ?>
                </ul>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>

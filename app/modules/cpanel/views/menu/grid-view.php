<?php Pjax::begin();

use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\widgets\Pjax; ?>
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'label',
        'slug',
        [
            'attribute' => 'url',
            'value' => function ($model) {
                return Html::a($model->url, $model->url);
            },
            'format' => 'html'
        ],
        [
            'attribute'=>'parent.label',
            'label'=>Yii::t('app','Parent')
        ],
        'sort_order',

        ['class' => ActionColumn::className(),
            'header' => Yii::t('app', 'Actions'),
            'template' => '{view} {update} {delete}',
            'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a(
                        '<span class="material-icons" title="' . Yii::t('app', 'View menu data') . '">remove_red_eye</span>', $url);
                },
                'update' => function ($url, $model) {
                    return Html::a(
                        '<span class="material-icons" title="' . Yii::t('app', 'Update menu data') . '">mode_edit</span>', $url);
                },
                'delete' => function ($url, $model) {
                    return Html::a(
                        '<span class="material-icons" title="' . Yii::t('app', 'Delete menu data') . '">delete</span>', $url, [
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this menu?'),
                            'method' => 'post',
                        ],
                    ]);
                },
            ],

        ],
    ],
]); ?>
<?php Pjax::end(); ?>
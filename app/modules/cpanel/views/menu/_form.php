<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Menus */
/* @var $form yii\widgets\ActiveForm */
/* @var $menus app\models\Menus */

?>

<div class="menus-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'icon')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'permission')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent_id')->dropDownList($menus, ['maxlength' => true]) ?>

    <?= $form->field($model, 'has_child')->dropDownList([
        '0' => Yii::t('app', 'No child'),
        '1' => Yii::t('app', 'Have child')
    ], ['maxlength' => true]) ?>

    <?= $form->field($model, 'sort_order')->input('number', ['maxlength' => true]) ?>
    <ul class="collapsible" data-collapsible="accordion" style="padding: 0 15px;">
        <li>
            <div class="collapsible-header"><i
                        class="material-icons">filter_drama</i><?= Yii::t('app', 'Advanced settings ') ?></div>
            <div class="collapsible-body ">
                <p>
                    <?= $form->field($model, 'link_template')->textInput(['value' => '<a href="{url}" class="{class}" ><i class="material-icons">{icon}</i> {label}</a>']) ?>

                    <?= $form->field($model, 'class')->textInput(['value' => 'collapsible-header waves-effect waves-grey']) ?>

                    <?= $form->field($model, 'controller') ?>
                </p>
            </div>
        </li>

    </ul>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

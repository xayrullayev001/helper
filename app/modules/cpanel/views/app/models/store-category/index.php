<?php

use yii\helpers\Html;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\StoreCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Store Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="row" style="margin-bottom: 0px;">
                    <div class="page-title">
                        <?= Html::encode($this->title) ?>
                    </div>
                    <div class="page-title-buttons">
                        <?=                         Html::a(Yii::t('app', 'Create Store Category')                        , ['create'], ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                    <?php Pjax::begin(); ?>
                                        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                                                    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
        'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                                'id',
            'name',
            'created_at',
            'updated_at',

                    ['class' => ActionColumn::className(),
                        'header' => Yii::t('app', 'Actions'),
                        'template' => '{view} {update} {delete}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a(
                                    '<span class="material-icons" title="' . Yii::t('app', 'View faculty data') . '">remove_red_eye</span>', $url);
                            },
                            'update' => function ($url, $model) {
                                return Html::a(
                                    '<span class="material-icons" title="' . Yii::t('app', 'Update faculty data') . '">mode_edit</span>', $url);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a(
                                    '<span class="material-icons" title="' . Yii::t('app', 'Delete faculty data') . '">delete</span>', $url, [
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                            },
                        ],
                    ]
                    ],
                    ]); ?>
                                    <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StoreCategory */

$this->title = Yii::t('app', 'Create Store Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Store Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="row" style="margin-bottom: 0px;">
                    <div class="page-title">
                        <?= Html::encode($this->title) ?>
                    </div>
                    <div class="page-title-buttons">
                        <a class="btn btn-success " href="/gii">Orqaga</a>                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <?= $this->render('_form', [
                'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>

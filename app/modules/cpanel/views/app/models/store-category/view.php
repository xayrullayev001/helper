<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\StoreCategory */

$this->title = $model->name;
?>
<div class="row">
    <div class="col s12 m12 l12">
        <div class="row">
            <div class="card">
                <div class="card-content">
                    <div class="row" style="margin-bottom: 0px;">
                        <div class="page-title">
                                                    </div>
                        <div class="page-title-buttons">
                            <?= Html::a(Yii::t('app', 'Update'),['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a(Yii::t('app', 'Delete'),['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?')                                ,'method' => 'post',
                                ],]) ?>
                            <?= Html::a(Yii::t('app', 'Create Store Category'), ['create'], ['class' => 'btn btn-success']) ?>
                            <?= Html::a(Yii::t('app', 'Back'), ['index'], ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">


                <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                            'id',
            'name',
            'created_at',
            'updated_at',
                ],
                ]) ?>

            </div>
        </div>
    </div>
</div>


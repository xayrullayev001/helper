<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Products */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<style>
    .img-in-table {
        max-width: 100%;
    }
</style>
<div class="row">
    <div class="col s12 m12 l12">
        <div class="row">
            <div class="card">
                <div class="card-content">
                    <div class="row" style="margin-bottom: 0px;">
                        <div class="page-title">
                        </div>
                        <div class="page-title-buttons">
                            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'waves-effect waves-light btn btn ']) ?>
                             <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                                'class' => 'waves-effect waves-light btn',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'), 'method' => 'post',
                                ],]) ?>
                             <?= Html::a(Yii::t('app', 'Create Products'), ['create'], ['class' => 'waves-effect waves-light btn']) ?>
                             <?= Html::a(Yii::t('app', 'Back'), ['index'], ['class' => 'waves-effect waves-light btn']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">


                <?= DetailView::widget([
                    'model' => $model,
                    'options'=>[
                        'class'=>'table-responsive'
                    ],
                    'attributes' => [
                        'name',
                        'price',
                        'discount',
                        [
                            'attribute'=>'image',
                            'value'=>function($model){
                                return Html::img($model->image,['class'=>'img-in-table']);
                            },
                            'format'=>'html'
                        ],
                        'count',
                        'updated_at:date',
                        'created_at:date',
                        'comment',
                    ],
                ]) ?>

            </div>
        </div>
    </div>
</div>


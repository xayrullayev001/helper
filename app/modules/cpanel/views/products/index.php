<?php

use yii\helpers\Html;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .img-in-table > img {
        max-width: 15% !important;
    }
</style>
<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="row" style="margin-bottom: 0px;">
                    <div class="page-title">
                        <?= Html::encode($this->title) ?>
                    </div>
                    <div class="page-title-buttons">
                        <?= Html::a(Yii::t('app', 'Create Products'), ['create'], ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <?php Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
//                    'filterModel' => $searchModel,
                    'tableOptions'=>[
                        'class'=>'table-responsive'
                    ],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'image',
                            'headerOptions' => [
                                'class' => 'hide-on-small-only',
                                'style' => 'width:10%'
                            ],
                            'contentOptions' => [
                                'class' => 'hide-on-small-only img-in-table'
                            ],
                            'format' => 'image'
                        ],
                        [
                            'attribute' => 'name',
                            'headerOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                            'contentOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                        ],
                        [
                            'attribute' => 'name',
                            'headerOptions' => [
                                'class' => 'hide-on-med-and-up'
                            ],
                            'contentOptions' => [
                                'class' => 'hide-on-med-and-up'
                            ],
                            'value' => function ($model) {
                                return Html::a($model->name, ['view', 'id' => $model->id]);
                            },
                            'format' => 'html'
                        ],
                        [
                            'attribute' => 'price',
                            'format' => ['decimal', 3]
                        ],
                        [
                            'attribute' => 'discount',
                            'headerOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                            'contentOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                        ],
                        [
                            'attribute' => 'count',
                            'headerOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                            'contentOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                        ],
                        [
                            'class' => ActionColumn::className(),
                            'headerOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                            'contentOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                            'header' => Yii::t('app', 'Actions'),
                            'template' => '{view} {update} {delete}',
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="material-icons" title="' . Yii::t('app', 'View faculty data') . '">remove_red_eye</span>', $url);
                                },
                                'update' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="material-icons" title="' . Yii::t('app', 'Update faculty data') . '">mode_edit</span>', $url);
                                },
                                'delete' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="material-icons" title="' . Yii::t('app', 'Delete faculty data') . '">delete</span>', $url, [
                                        'data' => [
                                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                            'method' => 'post',
                                        ],
                                    ]);
                                },
                            ],
                        ]
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>

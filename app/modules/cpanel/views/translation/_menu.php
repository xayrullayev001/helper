<?php

use yii\helpers\Url;

$action = $this->context->action->id;
?>
<form>
    <ul class="nav nav-pills content-box">
        <li <?= ($action === 'index') ? 'class="active"' : '' ?>>
            <a class="waves-effect waves-light btn blue m-b-xs" href="<?= Url::to('/cpanel/translation/index') ?>">
                <?php if ($action != 'index') : ?>
                    <i class="glyph-icon icon-chevron-left font-12"></i>
                <?php endif; ?>
                <?= Yii::t('app', 'Translations') ?>
            </a>

            <a class="waves-effect waves-light btn blue m-b-xs" href="<?= Url::to('/cpanel/translation/create') ?>"><?= Yii::t('app', 'Add word') ?></a>
         <a class="waves-effect waves-light btn blue m-b-xs" href="<?= Url::to('/cpanel/translation/language') ?>"><?= Yii::t('app', 'Languages') ?></a>
        <div class="input-field col s4 m4 l4">
            <input type="text" name="q" class="form-control" placeholder="<?= Yii::t('app', 'Search') ?>"/>
            <button type="submit" class="waves-effect waves-light btn blue m-b-xs"><i class="material-icons dp48">search</i></button>
        </div>
        </li>
    </ul>
</form>

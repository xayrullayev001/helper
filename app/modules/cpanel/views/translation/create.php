<?php
$this->title = Yii::t('app', 'Create template');
?>

<div class="col s12">
    <div class="card">
        <div class="card-content">
            <?= $this->render('_menu') ?>
        </div>
    </div>
</div>

<div class="col s12 m12 l12">
    <div class="card">
        <div class="card-content">
            <?= $this->render('_form', ['model' => $model]) ?>
        </div>
    </div>
</div>

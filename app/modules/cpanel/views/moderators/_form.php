<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin();
    var_dump($model->getErrors());?>

    <?= $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username')->textInput([
                                            'autofocus' => true,
                                            'tabindex' => 2,
                                            'class'=>' required ',
                                        ]) ?>
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'avatar', ['template' =>
        '<div class="file-field input-field">
        <div class="btn teal lighten-1">
            <span>' . $model->getAttributeLabel('avatar') . '</span>
            {input}
        </div>
        <div class="file-path-wrapper">
            <input class="file-path validate valid" type="text">
        </div>
    </div>{error}'])->fileInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

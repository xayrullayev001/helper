<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Moderators');
?>
<div class="row">
    <div class="col s12 m12 l12">
        <div class="row">
            <div class="card">
                <div class="card-content">
                    <div class="row" style="margin-bottom: 0px;">
                        <div class="page-title">
                            <?= Html::encode($this->title) ?>
                        </div>
                        <div class="page-title-buttons">
                            <?= Html::a(Yii::t('app', 'Create moderator'), ['create'], ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
//                        'filterModel' => $searchModel,
                    'tableOptions' => ['class' => 'responsive-table'],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'fullname',
                        'username',
                        'created_at:date',
                        'authKey',
                        'authKey',
                        [
                            'label' => 'status',
                            'value' => function ($model) {
                                return $model->getStatus();
                            }
                        ],
                        [
                            'label' => Yii::t('app', 'Role'),
                            'value' => function ($model) {
                                $roles = Yii::$app->authManager->getRolesByUser($model->id);
                                return $roles[array_key_first($roles)]->description;
                            },
                            'format' => 'text'
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view} {update} {delete}',
                            'header' => Yii::t('app', 'Actions'),
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="material-icons" title="' . Yii::t('app', 'View user data') . '">remove_red_eye</span>', $url);
                                },
                                'update' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="material-icons" title="' . Yii::t('app', 'Update user data') . '">mode_edit</span>', $url);
                                },
                                'delete' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="material-icons" title="' . Yii::t('app', 'Delete user data') . '">delete</span>', $url, [
                                        'data' => [
                                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                            'method' => 'post',
                                        ],
                                    ]);
                                },
                            ]
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

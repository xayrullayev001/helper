<?php

use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/** @var $this yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

$this->title = Yii::t('app', 'Dashboard');

?>

<div class="row">

    <?php foreach ($dataProvider->models as $category) {
        if (count($category->stores) <= 1) continue;
        ?>
        <div class="row">
            <h2><?= $category->name ?></h2>
            <? foreach ($category->stores as $store) {
                ?>
                <div class="col s12 m3">
                    <div class="card small">
                        <div class="card-image">
                            <img src="<?= $store->logo ?>" alt="">
                        </div>
                        <div class="card-content">
                            <p><?= $store->name ?></p>
                            <p><?= Yii::t('app', 'Cashback: {cashback} %', ['cashback' => $store->cashback]) ?></p>
                        </div>
                        <div class="card-action">
                            <a href="<?= \yii\helpers\Url::to(['/cpanel/stores/products', 'id' => $store->id]) ?>"><?= Yii::t('app', 'View all products') ?></a>
                        </div>
                    </div>
                </div>
                <?
            } ?>
        </div>
    <? } ?>

    <? try {
        echo LinkPager::widget([
            'pagination' => $dataProvider->pagination,
        ]);
    } catch (Exception $e) {
        throw new $e;
    } ?>
</div>
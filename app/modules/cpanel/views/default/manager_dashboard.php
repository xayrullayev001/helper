<?php

use yii\widgets\LinkPager;

/** @var $this yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

$this->title = Yii::t('app', 'Dashboard');
?>

<div class="row">
    <? foreach ($dataProvider->models as $store) { ?>
        <div class="col s12 m3">
            <div class="card small">
                <div class="card-image">
                    <img src="<?= $store->logo ?>" alt="">
                </div>
                <div class="card-content">
                    <p><?= $store->name ?></p>
                    <p><?= Yii::t('app', 'Cashback: {cashback} %', ['cashback' => $store->cashback]) ?></p>
                </div>
                <div class="card-action">
                    <a href="<?= \yii\helpers\Url::to(['/cpanel/stores/products', 'id' => $store->id]) ?>"><?= Yii::t('app', 'View all products') ?></a>
                </div>
            </div>
        </div>
    <? } ?>
</div>

<? try {
    echo LinkPager::widget([
        'pagination' => $dataProvider->pagination,
    ]);
} catch (Exception $e) {
    throw new $e;
} ?>

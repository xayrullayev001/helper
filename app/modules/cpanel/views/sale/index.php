<?php

use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SaleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sale');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="row" style="margin-bottom: 0px;">
                    <div class="page-title">
                        <?= Html::encode($this->title) ?>
                    </div>
                    <div class="page-title-buttons">
                        <?= Html::a(Yii::t('app', 'Create Sale'), ['create'], ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <?php Pjax::begin(); ?>
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'salesman',
                            'headerOptions' => [
                                'class' => 'hide-on-med-and-up'
                            ],
                            'contentOptions' => [
                                'class' => 'hide-on-med-and-up'
                            ],
                            'value' => function ($model) {
                                return Html::a($model->salesMan->fullname, ['view', 'id' => $model->id]);
                            },
                            'format' => 'html'
                        ],
                        [
                            'attribute' => 'salesman',
                            'headerOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                            'contentOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                            'value' => function ($model) {
                                return $model->salesMan->fullname;
                            }

                        ],
                        [
                            'attribute' => 'product_id',
                            'headerOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                            'contentOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                        ],
                        [
                            'attribute' => 'summa',
                            'format' => ['decimal', 3]
                        ],
                        //'qr_code',
                        //'updated_at',
                        //'created_at',
                        //'store_id',

                        [
                            'class' => ActionColumn::className(),
                            'headerOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                            'contentOptions' => [
                                'class' => 'hide-on-small-only'
                            ],
                            'header' => Yii::t('app', 'Actions'),
                            'template' => '{view} {update} {delete}',
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="material-icons" title="' . Yii::t('app', 'View faculty data') . '">remove_red_eye</span>', $url);
                                },
                                'update' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="material-icons" title="' . Yii::t('app', 'Update faculty data') . '">mode_edit</span>', $url);
                                },
                                'delete' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="material-icons" title="' . Yii::t('app', 'Delete faculty data') . '">delete</span>', $url, [
                                        'data' => [
                                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                            'method' => 'post',
                                        ],
                                    ]);
                                },
                            ],
                        ]
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>

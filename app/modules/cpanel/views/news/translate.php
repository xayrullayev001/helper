<?php

use app\models\Lang;
use app\models\News;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="row">
    <div class="col s12">
        <div class="page-title"><?= Html::encode($this->title) ?></div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'lang')->hiddenInput()->label(false) ?>
                <?= $form->field($model, 'slug')->hiddenInput()->label(false) ?>
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'text')->widget(TinyMce::className(), [
                    'options' => ['rows' => 6],
                    'language' => 'ru',
                    'clientOptions' => [
                        'plugins' => [
                            "advlist autolink lists link charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media table contextmenu paste image imagetools"
                        ],

                        'menubar' => ["file", "edit", "insert", "view", "format", "table", "tools", "help"],
                        'automatic_uploads' => true,
                        'file_picker_types' => 'image',

                        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image imageupload | fontselect | cut copy paste  | preview"
                    ]
                ]); ?>

                <?= $form->field($model, 'picture', ['template' =>
                    '<div class="file-field input-field">
                        <div class="btn teal lighten-1">
                            <span>' . $model->getAttributeLabel('picture') . '</span>
                            {input}
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate valid" type="text">
                        </div>
                    </div>{error}'])->fileInput(['maxlength' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>

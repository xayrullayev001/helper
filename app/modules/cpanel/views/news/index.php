<?php

use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'News');
$this->params['breadcrumbs'][] = $this->title;


?>
<style>
    .lang-label {
        padding: 5px 15px;
        color: white;
        border-radius: 3px;
    }
</style>
<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="row" style="margin-bottom: 0px;">
                    <div class="page-title">
                        <?= Html::encode($this->title) ?>
                    </div>
                    <div class="page-title-buttons">
                        <?= Html::a(Yii::t('app', 'Create News'), ['create'], ['class' => 'btn btn-success ']) ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">

                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
//                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'picture',
                            'headerOptions' => ['class' => 'col s3 m3 l3'],
                            'contentOptions' => ['style' => 'width:20%'],
                            'value' => function ($model) {
                                return Html::img($model->picture, ['class' => 'materialboxed responsive-img initialized']);
                            },
                            'format' => 'html'
                        ],
                        [
                            'attribute' => 'title',
//                'headerOptions' => ['class'=>'col s3 m3 l3'],
                            'contentOptions' => ['style' => 'width:25%'],
                        ],
                        [
                            'attribute' => 'lang',
                            'headerOptions' => ['class' => 'col s3 m3 l3'],
                            'contentOptions' => ['style' => 'width:30%'],
                            'value' => function ($model) {
                                return $model->generateLangBadges();
                            },
                            'format' => 'html'
                        ],
                        ['class' => ActionColumn::className(),
                            'header' => Yii::t('app', 'Actions'),
                            'template' => '{view} {update} {delete}',
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="material-icons" title="' . Yii::t('app', 'View news data') . '">remove_red_eye</span>', $url);
                                },
                                'update' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="material-icons" title="' . Yii::t('app', 'Update news data') . '">mode_edit</span>',  $url);
                                },
                                'delete' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="material-icons" title="' . Yii::t('app', 'Delete news data') . '">delete</span>',  $url, [
                                        'data' => [
                                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                            'method' => 'post',
                                        ],
                                    ]);
                                },
                            ],

                        ]
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

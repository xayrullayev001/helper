<?php

use app\models\Lang;
use app\models\News;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */
$myDateTime = new DateTime();
$content_css = [
    '/backend/assets/3be5f0f5/css/bootstrap.css?' . $myDateTime->getTimestamp(),];
?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'lang')->hiddenInput(['value' => Lang::getDefaultLangInSystem()])->label(false) ?>
<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'text')->widget(TinyMce::className(), [
    'language' => Yii::$app->language,

    'clientOptions' => [
        'selector' => "textarea",  // change this value according to your HTML
        //set br for enter
        'file_picker_callback' => new JsExpression("function(cb, value, meta) {
             var input = document.createElement('input');
             input.setAttribute('type', 'file');
             input.setAttribute('accept', 'image/*');
             
             // Note: In modern browsers input[type=\"file\"] is functional without 
             // even adding it to the DOM, but that might not be the case in some older
             // or quirky browsers like IE, so you might want to add it to the DOM
             // just in case, and visually hide it. And do not forget do remove it
             // once you do not need it anymore.

         input.onchange = function() {
           var file = this.files[0];
   
           var reader = new FileReader();
           reader.onload = function () {
             // Note: Now we need to register the blob in TinyMCEs image blob
             // registry. In the next release this part hopefully won't be
             // necessary, as we are looking to handle it internally.
             var id = 'blobid' + (new Date()).getTime();
             var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
             var base64 = reader.result.split(',')[1];
             var blobInfo = blobCache.create(id, file, base64);
             blobCache.add(blobInfo);
        
             // call the callback and populate the Title field with the file name
             cb(blobInfo.blobUri(), { title: file.name });
           };
           reader.readAsDataURL(file);
         };
         
         input.click();
        }"),
        'plugins' => [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste image imagetools"
        ],

        'menubar' => ["file", "edit", "insert", "view", "format", "table", "tools", "help"],
        'automatic_uploads' => true,
        'file_picker_types' => 'image',

        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image imageupload | fontselect | cut copy paste  | preview"
    ]
]); ?>

<?= $form->field($model, 'picture', ['template' =>
    '<div class="file-field input-field">
        <div class="btn teal lighten-1">
            <span>' . $model->getAttributeLabel('picture') . '</span>
            {input}
        </div>
        <div class="file-path-wrapper">
            <input class="file-path validate valid" type="text">
        </div>
    </div>{error}'])->fileInput(['maxlength' => true]) ?>

<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>

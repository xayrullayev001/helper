<?php

namespace app\modules\cpanel\controllers;

use app\models\SoldSearch;
use app\models\SaleSearch;
use app\models\StoreCategorySearch;
use app\models\User;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `cpanel` module
 */
class DefaultController extends Controller
{


    public function actionIndex()
    {
        $user = Yii::$app->user->identity;
        if (Yii::$app->authManager->checkAccess($user->getId(),"Manager-stores")) {
            $searchModel = new StoreCategorySearch();
            $dataProvider = $searchModel->searchManagerStores(Yii::$app->request->queryParams,$user->getId());

            return $this->render('manager_dashboard', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else
            if ($user->is_client == User::USER_REGISTERED_CLIENT) {
                $searchModel = new StoreCategorySearch();
                $dataProvider = $searchModel->searchWithStores(Yii::$app->request->queryParams);

                return $this->render('client_dashboard', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]);
            } else {
                $searchModel = new SaleSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]);
            }
    }

    public function actionScan()
    {
        return $this->render("scan");
    }
}

<?php

namespace app\modules\cpanel\controllers;

use app\components\CRUDController;
use app\components\CRUDWithCompanyController;
use app\models\Sale;
use Yii;

/**
 * SoldController implements the CRUD actions for Sold model.
 */
class SaleController extends CRUDWithCompanyController
{
    public $model = 'Sale';
    public $image = false;

    /**
     * Creates a new Sold model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sale();
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post('Sold');
            $model->summa = $post['summa'];
            $model->product_id = (is_array($post['product_id'])) ? implode(",", $post['product_id']) : $post['product_id'];
            $model->store_id = Yii::$app->user->identity->store_id;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionScan()
    {
        $model = new Sale();
        return $this->render('scan', [
            'model' => $model,
        ]);
    }
}

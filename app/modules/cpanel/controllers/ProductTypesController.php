<?php

namespace app\modules\cpanel\controllers;

use app\components\CRUDController;

/**
 * ProductTypesController implements the CRUD actions for ProductTypes model.
 */
class ProductTypesController extends CRUDController
{
    public $model = "ProductTypes";
    public $image = false;
}

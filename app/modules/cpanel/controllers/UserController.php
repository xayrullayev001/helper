<?php

namespace app\modules\cpanel\controllers;

use app\components\auth\models\AuthItem;
use app\components\CRUDController;
use app\models\User;
use app\models\UserSearch;
use Throwable;
use Yii;
use yii\base\Exception;
use yii\db\StaleObjectException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\rbac\Role;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends CRUDController
{
    public $model = 'User';
    public $image = "avatar";

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ["{$this->model}-index"],
                        'actions' => ['index']
                    ],
                    [
                        'allow' => true,
                        'roles' => ["{$this->model}-create"],
                        'actions' => ['create']
                    ],
                    [
                        'allow' => true,
                        'roles' => ["{$this->model}-update"],
                        'actions' => ['update']
                    ],
                    [
                        'allow' => true,
                        'roles' => ["{$this->model}-delete"],
                        'actions' => ['delete']
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchByStoreId(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }
        Yii::$app->session->setFlash('success', Yii::t('app', 'Something went wrong'));
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws Exception
     */
    public function actionCreate()
    {
        $model = new User();

        if ($post = Yii::$app->request->post('User')) {
            $model->username = $post['username'];
            $model->fullname = $post['fullname'];
            $model->generateAuthKey();
            $model->generateToken();
            $model->status = User::USER_CREATED;
            $model->store_id = $post['store_id'];
            $model->setPassword($post['password']);

            $model->store_id = Yii::$app->user->identity->store_id;
            if ($model->validate() && $this->fileUpload($model, $this->image, strtolower($this->model)) && $model->save()) {
                $role = Yii::$app->authManager->getRole("SALESMAN");
                Yii::$app->authManager->assign($role, $model->id);
                Yii::$app->session->setFlash('success', Yii::t('app', 'You  successfully registered new account!'));

                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Somthing went wrong!'));
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->password = "";
        if ($post = Yii::$app->request->post('User')) {
            $model->username = $post['username'];
            $model->fullname = $post['fullname'];
            $model->status = User::USER_CREATED;
            $model->setPassword($post['password']);
            if ($model->validate() && $this->fileUpload($model, $this->image, strtolower($this->model)) && $model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'User account seccess updated!'));
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Somthing went wrong!'));
                return $this->render('update', [
                    'model' => $model,
                ]);
            }

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', Yii::t('app', 'User account seccess deleted!'));
        return $this->redirect(['index']);
    }

    public function actionCreateManager()
    {
        $model = new User();

        if ($post = Yii::$app->request->post('User')) {
            $model->username = $post['username'];
            $model->fullname = $post['fullname'];
            $model->generateAuthKey();
            $model->generateToken();
            $model->setPassword($post['password']);
            $model->status = User::USER_CREATED;
            $model->setQrCode();
            $model->store_id = 0;
            $model->open_id = rand(11111, 99999);
            if ($model->validate() && $model->save()) {
                $role = Yii::$app->authManager->getRole("MANAGER");
                Yii::$app->authManager->assign($role, $model->id);
                Yii::$app->session->setFlash('success', Yii::t('app', 'You successfully registered new manager account!'));
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Somthing went wrong!'));
                return $this->render('create-manager', [
                    'model' => $model,
                ]);
            }
        }
        return $this->render('create-manager', [
            'model' => $model,
        ]);
    }

    public function actionManagers()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchByRole(Yii::$app->request->queryParams, "MANAGER");

        return $this->render('managers', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionViewManager($id)
    {
        return $this->render('view-manager', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionUpdateManager($id)
    {
        $model = $this->findModel($id);

        if ($post = Yii::$app->request->post('User')) {
            $model->username = $post['username'];
            $model->fullname = $post['fullname'];
            $model->setPassword($post['password']);
            if ($model->validate() && $model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'You successfully registered new manager account!'));
                return $this->redirect(['view-manager', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Somthing went wrong!'));
                return $this->render('update-manager', [
                    'model' => $model,
                ]);
            }
        }
        return $this->render('update-manager', [
            'model' => $model,
        ]);
    }

    public function actionDeleteManager($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Manager account secess deleted!'));
        return $this->redirect(['index']);
    }

    public function actionPermit($id)
    {
        $model = new \app\components\auth\models\AuthAssignment();
        $roles = AuthItem::find()->where(['type'=>AuthItem::TYPE_ROLE,'name' => "SALESMAN"])->all();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->save()) {
                return $this->redirect(['index']);
            }else{
                return $this->render("permit", [
                    'model' => $model,
                    'user' => $id,
                    'roles' => $roles
                ]);
            }
        }
        return $this->render("permit", [
            'model' => $model,
            'user' => $id,
            'roles' => $roles
        ]);
    }
}

<?php

namespace app\modules\cpanel\controllers;

use app\models\News;
use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;

class FileController extends Controller
{
    public function actionUpload(){
        $url = array(
            "http://uzdjtsu/"
        );

        reset($_FILES);
        $temp = current($_FILES);

        if (is_uploaded_file($temp['tmp_name'])) {
            if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
                header("HTTP/1.1 400 Invalid file name,Bad request");
                return;
            }
            
            // Validating File extensions
            if (! in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array(
                "gif",
                "jpg",
                "png"
            ))) {
                header("HTTP/1.1 400 Not an Image");
                return;
            }
            
            $fileName = "uploads/" . $temp['name'];
            move_uploaded_file($temp['tmp_name'], $fileName);
            
            // Return JSON response with the uploaded file path.
            return json_encode(array(
                'location' => $fileName
            ));
        }


        // Yii::$app->response->format = Response::FORMAT_JSON;

        // return JSON::encode(['location'=>"test/url"]);
        // $model = new News();
        // if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        //     $model->file = UploadedFile::getInstance($model, 'picture');
        //     $model->file->saveAs('uploads/signature/' . time() . '.' . $model->picture->extension);
        //     $url = 'uploads/signature/' . time() . 'upload.' . $model->picture->extension;
        //     return ['success'=>"/uploads/sections/IMG_0148.jfif"];

        // }else{
        //     return ['error'=>"error"];
        // }
    }
}
<?php

namespace app\modules\cpanel\controllers;

use app\components\CRUDController;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends CRUDController
{
    public $model = "Products";
    public $image = "image";
}

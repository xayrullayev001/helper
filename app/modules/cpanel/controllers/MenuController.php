<?php

namespace app\modules\cpanel\controllers;

use app\components\CRUDController;
use app\components\SimpleGenerator;
use app\models\Menus;
use Yii;
use yii\gii\CodeFile;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * MenuController implements the CRUD actions for Menus model.
 */
class MenuController extends CRUDController
{
    public $model = 'Menus';

    /**
     * Lists all Menus models.
     * @return mixed
     */
    public function actionIndex()
    {
//        $searchModel = new MenusSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $menuItems = Menus::find()->with("withChilds")->orderBy(['sort_order' => SORT_ASC])->all();

        return $this->render('index', [
//            'searchModel' => $searchModel,
            'menuItems' => $menuItems,
        ]);
    }

    /**
     * Displays a single Menus model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Menus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menus::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Creates a new Menus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\Exception
     * @throws \Exception
     */
    public function actionCreate()
    {
        $model = new Menus();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->permission != Menus::PUBLIC) {
                $authManager = Yii::$app->authManager;
                $role = $authManager->getRolesByUser(Yii::$app->user->identity->getId());
                if ($authManager->hasChild($role[array_key_first($role)], $authManager->createPermission($model->permission)) == null) {
                    $authManager->add($authManager->createPermission($model->permission));
                    $newPermission = $authManager->getPermission($model->permission);
                    if ($authManager->canAddChild($role[array_key_first($role)], $newPermission)) {
                        $authManager->addChild($role[array_key_first($role)], $newPermission);
                    }
                }
                if (count(explode("/", $model->url)) > 2) {
                    (new SimpleGenerator($model->url))->generateController();
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }
        $menu1 = ArrayHelper::map(Menus::find()->where(['status' => 1])->asArray()->all(), 'id', 'label');
        array_push($menu1, ['null' => Yii::t('app', "Not selected")]);
        return $this->render('create', [
            'model' => $model,
            'menus' => $menu1
        ]);
    }


    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->permission != Menus::PUBLIC) {
                $authManager = Yii::$app->authManager;
                $role = $authManager->getRolesByUser(Yii::$app->user->identity->getId());
                if (is_array($role) && $authManager->hasChild($role[array_key_first($role)], $authManager->createPermission($model->permission)) == null) {
                    $authManager->add($authManager->createPermission($model->permission));
                    $newPermission = $authManager->getPermission($model->permission);
                    if ($authManager->canAddChild($role[array_key_first($role)], $newPermission)) {
                        $authManager->addChild($role[array_key_first($role)], $newPermission);
                    }
                }
            }
            if (count(explode("/", $model->url)) > 2) {

                (new SimpleGenerator($model->url))->generateController();
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }
        $menu1 = ArrayHelper::map(Menus::find()->where(['status' => 1])->asArray()->all(), 'id', 'label');
        array_push($menu1, ['null' => Yii::t('app', "Not selected")]);
        return $this->render('update', [
            'model' => $model,
            'menus' => $menu1
        ]);
    }

    /**
     * Deletes an existing Menus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}

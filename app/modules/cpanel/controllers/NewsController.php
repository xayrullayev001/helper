<?php

namespace app\modules\cpanel\controllers;

use app\components\CRUDController;
use app\models\News;
use Yii;
use yii\base\Model;
use app\models\Lang;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends CRUDController
{
    public $model = 'News';
    public $image = 'picture';
    public function actionIndex()
    {
        $m = '\\app\\models\\' . $this->model.'Search';
        $searchModel = new $m();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,Lang::getDefaultLangInSystem());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTranslate($slug, $lang)
    {
        $model = News::find()->where(['slug' => $slug, 'lang' => $lang])->one();
        if ($model != null) {
            return $this->redirect(['news/update', 'id' => $model->id]);
        } else {
            return $this->redirect(['news/add-translation', 'slug' => $slug, 'lang' => $lang]);
        }
    }

    public function actionAddTranslation($slug, $lang)
    {
        $modelExistsCheck = News::findOne(['slug' => $slug]);
        $model = new News();
        $model->slug = $slug;
        $model->lang = $lang;
        $model->picture = $modelExistsCheck->picture;
       if($post = Yii::$app->request->post()){
           $model->load($post);
           $model->picture = $modelExistsCheck->picture;
           if($model->save()){  
                return $this->redirect(['news/index']);
           }
           else{
                return $this->render('translate', [
                    'model' => $model
                ]);
           }
       }

        return $this->render('translate', [
            'model' => $model
        ]);
    }
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        parent::actionDelete($id);
        News::deleteAll(['slug'=>$model->slug]);
    }

}

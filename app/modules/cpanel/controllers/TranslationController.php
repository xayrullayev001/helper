<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 02.06.2019
 * Time: 21:04
 */

namespace app\modules\cpanel\controllers;

use yii\web\Controller;

use app\models\Lang;
use app\models\Message;
use app\models\SourceMessage;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


class TranslationController extends Controller
{
    /**
     * @var
     */
    public $model = "Translate";


    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $action
     * @return bool
     * @throws ForbiddenHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (\Yii::$app->user->can("{$this->model}-{$action->id}")) {
            return parent::beforeAction($action);
        } else {
            throw new ForbiddenHttpException("Access denied");
        }
    }


    public function actionIndex()
    {

        $query = SourceMessage::find()->joinWith('messages')->orderBy(['id' => SORT_DESC]);
        if (isset($_GET['q'])) {
            $query->orFilterWhere(['like', "`message`.`translation`", $_GET['q']]);
            $query->orFilterWhere(['like', 'message', $_GET['q']]);
        }
        $data = new ActiveDataProvider([
            'query' => $query,
        ]);
        Yii::$app->user->setReturnUrl(Url::to('/cpanel/translation/index'));

        return $this->render('index', [
            'data' => $data
        ]);
    }

    public function actionCreate()
    {
        $model = new SourceMessage();

        if ($model->load(\Yii::$app->request->post())) {
            if (\Yii::$app->request->isAjax) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if ($model->save()) {
                    Yii::$app->session->setflash('success', Yii::t('app', 'Message source is created'));
                    return $this->redirect(Url::to('/cpanel/translation/index'));
                } else {
                    Yii::$app->session->setflash('error', Yii::t('app', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        } else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = SourceMessage::findOne($id);
        if ($model === null) {
            Yii::$app->session->setflash('error', \Yii::t('app', 'Not found'));
            return $this->redirect(Url::to('/cpanel/translation/index'));
        }
        if ($model->load(\Yii::$app->request->post())) {
            if (\Yii::$app->request->isAjax) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if ($model->save()) {
                    Yii::$app->session->setflash('success', Yii::t('app', 'Message is updated'));
                } else {
                    Yii::$app->session->setflash('error', Yii::t('app', 'Update error. {0}', $model->formatErrors()));
                }
                return $this->refresh();
            }
        } else {
            return $this->render('edit', [
                'model' => $model
            ]);
        }

    }

    public function actionSave($id, $lang)
    {

        $model = Message::find()->where(['id' => $id, 'language' => $lang])->one();
        if (!$model) $model = new Message();
        if (Yii::$app->request->isPost) {
            if ($post = \Yii::$app->request->post() && $model->load(\Yii::$app->request->post())) {
                if ($model->save()) {

                    Yii::$app->session->setFlash('success', Yii::t('app', 'Message successfully saved!'));
                    return $this->redirect('/cpanel/translation/index');
                } else {
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Something wentwrong'));

                    return $this->render('add', [
                        'model' => $model,
                        'id' => $id,
                        'lang'=>$lang
                    ]);
                }
            }
        }

        return $this->render('add', [
            'model' => $model,
            'id' => $id,
            'lang'=>$lang
        ]);
    }

    public function actionLanguage()
    {
        $data = new ActiveDataProvider([
            'query' => Lang::find(),
        ]);
        \Yii::$app->user->setReturnUrl(Url::to('/cpanel/translation/languages'));

        return $this->render('language', [
            'data' => $data
        ]);
    }

    public function actionLanguageedit($id)
    {
        $model = Lang::findOne(['id' => $id]);
        if ($model === null) {
            Yii::$app->session->setflash('error', \Yii::t('app', 'Not found'));
            return $this->redirect(Url::to('/cpanel/translation/index'));
        }
        if ($model->load(\Yii::$app->request->post())) {
            if (\Yii::$app->request->isAjax) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if ($model->save()) {
                    Yii::$app->session->setflash('success', \Yii::t('app', 'Language is updated'));
                } else {
                    Yii::$app->session->setflash('error', \Yii::t('app', 'Update error. {0}', $model->formatErrors()));
                }
                return $this->refresh();
            }
        } else {
            return $this->render('langedit', [
                'model' => $model
            ]);
        }
    }

    /**
     * @param $id
     * @return Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = SourceMessage::find()->where(['id' => $id])->one();
        $model->delete();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Message successfully deleted!'));

        return $this->redirect(['index']);
    }
}
<?php

namespace app\modules\cpanel\controllers;

use app\components\CRUDController;
use app\models\Products;
use app\models\ProductTypes;
use app\models\Stores;
use yii\data\ActiveDataProvider;

/**
 * StoresController implements the CRUD actions for Stores model.
 */
class StoresController extends CRUDController
{
    public $model = "Stores";
    public $image = 'logo';

    public function actionProducts($id)
    {
        $this->layout = "/products";
        $store = Stores::findOne(['id'=>$id]);
        $products = ProductTypes::find()
            ->joinWith(['products','products.store'])
            ->where(['products.store_id'=>$store->id])
            ->andWhere(['>','price',$store->average_check]);

        $dataProvider = new ActiveDataProvider([
           'query'=> $products
        ]);

        return $this->render('products',[
            'dataProvider'=>$dataProvider,
            'store'=>$store,
        ]);
    }
}

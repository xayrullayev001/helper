<?php

namespace app\modules\cpanel\controllers;

use app\components\CRUDController;
use app\models\User;
use app\models\UserSearch;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class ModeratorsController extends CRUDController
{

    public $model = 'User';
    public $image = "avatar";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'pageCache' => [
                'class' => 'yii\filters\PageCache',
                'only' => ['index', 'view'],
                'duration' => 5,
                'varyByRoute' => true,
                'variations' => [
                    \Yii::$app->language,
                    Yii::$app->getUser()->getId()
                ]
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchByStoreId(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws Exception
     * @throws \Exception
     */
    public function actionCreate()
    {
        $model = new User();

        if ($post = Yii::$app->request->post('User')) {
            $model->username = $post['username'];
            $model->fullname = $post['fullname'];
            $model->generateAuthKey();
            $model->generateToken();
            $model->status = User::USER_CREATED;
            $model->setPassword($post['password']);
            $model->store_id = Yii::$app->user->identity->store_id;
            $model->setQrCode();
            if ($model->validate() && $this->fileUpload($model, $this->image, strtolower($this->model)) && $model->save()) {
                $role = Yii::$app->authManager->getRole("MODERATOR");
                Yii::$app->authManager->assign($role, $model->id);
                Yii::$app->session->setFlash('success', Yii::t('app', 'You successfully registered new moderator account!'));
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Somthing went wrong!'));
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->password = "";
        if ($post = Yii::$app->request->post('User')) {
            $model->username = $post['username'];
            $model->fullname = $post['fullname'];
            $model->status = User::USER_CREATED;
            $model->setPassword($post['password']);
            if ($model->validate() && $this->fileUpload($model, $this->image, strtolower($this->model)) && $model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Moderator account seccess updated!'));
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Somthing went wrong!'));
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionQrcode($id)
    {
        $query = User::findOne(['id' => $id]);
        return $query->authKey;
    }

}

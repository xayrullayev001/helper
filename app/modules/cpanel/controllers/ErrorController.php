<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 12.09.2019
 * Time: 0:18
 */

namespace app\modules\cpanel\controllers;


use yii\web\Controller;

class ErrorController extends Controller
{
    /**
     * @param $error
     * @return string
     */
    public function handle($error)
    {
        return $this->render("error");
    }
}
<?php

namespace app\modules\cpanel;

/**
 * cpanel module definition class
 */
class CPanel extends \yii\base\Module
{
    public $layout = "/cpanel";
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\cpanel\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
    public $newFileMode = 0666;
    /**
     * @var int the permission to be set for newly generated directories.
     * This value will be used by PHP chmod function.
     * Defaults to 0777, meaning the directory can be read, written and executed by all users.
     */
    public $newDirMode = 0777;
}

<?php

namespace app\modules\simple\controllers;

use Yii;
use yii\base\DynamicModel;
use yii\gii\CodeFile;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Default controller for the `simple` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * $model = new DynamicModel(compact('name', 'email'));
     * $model->addRule(['name', 'email'], 'string', ['max' => 128])
     * ->addRule('email', 'email')
     * ->validate();
     * @return string
     */
    public function actionIndex()
    {


        $dynamicModel = new DynamicModel(['model', 'name', 'type', 'count', 'visible']);
        $dynamicModel->addRule(['name', 'type'], 'string', ['max' => 50])
            ->addRule(['visible'], 'boolean')
            ->addRule(['model'], 'string')
            ->addRule(['name', 'type', 'visible'], 'required')
            ->validate(['name', 'type', 'visible']);
        $dynamicModel->generateAttributeLabel('name');
        if ($post = Yii::$app->request->post()) {
            $content = $this->renderPartial('app/modules/simple/migration/migration', ['model' => $post['DynamicModel']['model'], 'attributes' => $post['DynamicModel']]);
            $files = new CodeFile(
                "app/migrations/m".microtime()."_create_table_".$post['model'],
                $content
            );
            $files->save();

        }
        return $this->render('index', [
            'model' => $dynamicModel,
            'forms' => 1
        ]);
    }

    public function actionGetRow($id)
    {
        ++$id;
        $this->layout = false;
        $dynamicModel = new DynamicModel(['name', 'type', 'count', 'visible']);
        $dynamicModel->addRule(['name', 'type'], 'string', ['max' => 50])
            ->addRule(['visible'], 'boolean')
            ->addRule(['name', 'type', 'visible'], 'required')
            ->validate(['name', 'type', 'visible']);
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $row = $this->render('form/row', ['model' => $dynamicModel, 'i' => $id, 'form' => new ActiveForm()]);
            return ['form' => $row];
        }
    }
}

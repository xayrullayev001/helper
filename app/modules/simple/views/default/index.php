<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Menus */
/* @var $menus app\models\Menus */
/* @var $forms integer */

$this->title = Yii::t('app', 'Simple Generator');

?>

<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="row" style="margin-bottom: 0px;">
                    <div class="page-title">
                        <?= Html::encode($this->title) ?>
                    </div>
                    <div class="page-title-buttons">
                        <? #= Html::a(Yii::t('app', 'Back'), ['index'], ['class' => 'btn btn-success ']) ?>
                        <span  class="btn"  id="dynamic-model-row-add"><i class="material-icons">library_add</i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <?= $this->render('_form', [
                    'model' => $model,
                    'forms'=>$forms
                ]) ?>
            </div>
        </div>
    </div>
</div>


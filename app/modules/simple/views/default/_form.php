<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $menus \yii\base\DynamicModel */
$i = 1;
$this->registerJs('
    var row = $(".dynamic-model-row");
    var button = $("#dynamic-model-row-add");
    
     button.click(function(){
         $.get("'. Url::to(['get-row','id'=>$forms]).'",function(res){
                console.log("Response ",$("#dynamic-model-row"))
                $("#dynamic-model-row").append(res.form)
         });
    });
');

?>
<?php $form = ActiveForm::begin(); ?>


<div class="row " id="dynamic-model-row">

        <div class="col s12 m12 l12">
            <div class="card white">
                <div class="card-content center">
                    <p><?= $form->field($model, "model")->textInput(['maxlength' => true]) ?></p>
                </div>
            </div>
        </div>

    <div class="col s12 m6 l3">
        <div class="card white">
            <div class="card-content center">
                <p><?= $form->field($model, "[{$i}]name")->textInput(['maxlength' => true]) ?></p>
            </div>
        </div>
    </div>
    <div class="col s12 m6 l3">
        <div class="card white">
            <div class="card-content center">
                <p><?= $form->field($model, "[{$i}]type")->textInput(['maxlength' => true]) ?>
                </p>
            </div>
        </div>
    </div>
    <div class="col s12 m6 l3">
        <div class="card white">
            <div class="card-content center">
                <p><?= $form->field($model, "[{$i}]count")->textInput(['maxlength' => true]) ?></p>
            </div>
        </div>
    </div>
    <div class="col s12 m6 l3">
        <div class="card white">
            <div class="card-content center">
                <p><?= $form->field($model, "[{$i}]visible")->textInput(['maxlength' => true]) ?></p>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>



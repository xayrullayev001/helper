<?php

/**
 * @var $i integer
 */

?>

<div class="col s12 m6 l3">
    <div class="card white">
        <div class="card-content center">
            <p><?= $form->field($model, "[{$i}]name")->textInput(['maxlength' => true])?></p>
        </div>
    </div>
</div>
<div class="col s12 m6 l3">
    <div class="card white">
        <div class="card-content center">
            <p><?= $form->field($model, "[{$i}]type")->textInput(['maxlength' => true]) ?>
            </p>
        </div>
    </div>
</div>
<div class="col s12 m6 l3">
    <div class="card white">
        <div class="card-content center">
            <p><?= $form->field($model, "[{$i}]count")->textInput(['maxlength' => true]) ?></p>
        </div>
    </div>
</div>
<div class="col s12 m6 l3">
    <div class="card white">
        <div class="card-content center">
            <p><?= $form->field($model, "[{$i}]visible")->textInput(['maxlength' => true])?></p>
        </div>
    </div>
</div>




<?php
namespace app\modules\simple\migration;
/**
 * @var $attributes \yii\base\DynamicModel
 * @var $model \yii\base\DynamicModel
 */

echo "<?php\n";
?>

namespace app\migrations;

use Yii;

class m<?php echo microtime()?>_create_table_<?php echo $model?> extends \yii\db\Migration
{

    public function up()
    {
        $dataType = $this->binary();
        $tableOptions = null;

        switch ($this->db->driverName) {
            case 'mysql':
                // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
                break;
            case 'sqlsrv':
            case 'mssql':
            case 'dblib':
                $dataType = $this->text();
                break;
        }

        $this->createTable('{{%<?php echo $model?>}}', [
<?php for ($i=1;$i<count($attributes);$i++){?>
             <?php echo "{$attributes[$i]['name']}"?> => \yii\db\Schema::<?php echo $attributes[$i]['type']?>,
<?php}?>
        ], $tableOptions);
    }


    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%<?php echo $model?>}}');
    }

}
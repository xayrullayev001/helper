<?php


namespace app\assets;


use yii\web\AssetBundle;

class ProductsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
//    <!-- PLUGINS CSS STYLE -->
        "http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css",
        "web/plugins/ggrid/fonts/pixelfabric-clothes/style.css",
        "web/plugins/ggrid/css/demo.css",
        "web/plugins/ggrid/css/flickity.css",
        "web/plugins/ggrid/css/component.css",
    ];
    public $js = [
        "web/plugins/ggrid/js/modernizr.custom.js",
        "web/plugins/ggrid/js/isotope.pkgd.min.js",
        "web/plugins/ggrid/js/flickity.pkgd.min.js",
        "web/plugins/ggrid/js/main.js",
    ];

}

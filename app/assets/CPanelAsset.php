<?php


namespace app\assets;


use yii\web\AssetBundle;

class CPanelAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
//            <!-- Styles -->
        "web/plugins/materialize/css/materialize.min.css",
        "web/css/material-icons.css",
        "web/plugins/material-preloader/css/materialPreloader.min.css",
        "web/plugins/select2/css/select2.css",


//    <!-- Theme Styles -->
        "web/css/alpha.min.css",
        "web/css/custom.css",

    ];
    public $js = [
        "web/plugins/jquery/jquery-2.2.0.min.js",
        "web/plugins/materialize/js/materialize.min.js",
        "web/plugins/material-preloader/js/materialPreloader.min.js",
        "web/plugins/jquery-blockui/jquery.blockui.js",
        "web/plugins/counter-up-master/jquery.counterup.min.js",
        "web/js/alpha.min.js",
        "web/js/instascan.min.js",
    ];

    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
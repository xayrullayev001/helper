<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 18.02.2019
 * Time: 20:29
 */

namespace app\assets;


use yii\web\AssetBundle;

class SignAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'web/plugins/materialize/css/materialize.min.css',
        'web/css/material-icons.css',
        'web/plugins/material-preloader/css/materialPreloader.min.css',
        'web/css/alpha.min.css',
        'web/css/custom.css',
    ];
    public $js = [
        'web/plugins/jquery/jquery-2.2.0.min.js',
        'web/plugins/materialize/js/materialize.min.js',
        'web/plugins/material-preloader/js/materialPreloader.min.js',
        'web/plugins/jquery-blockui/jquery.blockui.js',
        'web/plugins/jquery-inputmask/jquery.inputmask.bundle.js',
        'web/js/alpha.min.js',
        'web/js/pages/form-input-mask.js',
    ];
    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
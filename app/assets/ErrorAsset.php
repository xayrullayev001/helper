<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 10.01.2020
 * Time: 20:39
 */

namespace app\assets;

use yii\web\AssetBundle;


class ErrorAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        "src/back/assets/plugins/materialize/css/materialize.min.css",
//        "src/back/assets/css/material-icons.css",
        "http://fonts.googleapis.com/icon?family=Material+Icons",
        "src/back/assets/plugins/material-preloader/css/materialPreloader.min.css",
        "src/back/assets/css/alpha.min.css",
        "src/back/assets/css/custom.css",
    ];
    public $js = [
        "src/back/assets/plugins/jquery/jquery-2.2.0.min.js",
        "src/back/assets/plugins/materialize/js/materialize.min.js",
        "src/back/assets/plugins/material-preloader/js/materialPreloader.min.js",
        "src/back/assets/plugins/jquery-blockui/jquery.blockui.js",
        "src/back/assets/js/alpha.min.js",
    ];
    public $depends = [];



}
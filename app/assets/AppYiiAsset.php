<?php


namespace app\assets;


use yii\web\AssetBundle;

class AppYiiAsset  extends AssetBundle
{
    public $sourcePath = '@yii/assets';
    public $js = [
        'yii.js',
    ];

}
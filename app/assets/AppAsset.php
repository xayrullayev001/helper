<?php


namespace app\assets;

use yii\web\AssetBundle;


class AppAsset extends AssetBundle
{
    public $sourcePath = '@webroot/src/app/';

    public $css = [
//    <!-- PLUGINS CSS STYLE -->
        "/src/app/plugins/bootstrap/css/bootstrap.min.css",
        "/src/app/plugins/selectbox/select_option1.css",
        "/src/app/plugins/font-awesome/css/font-awesome.min.css",
        "/src/app/plugins/flexslider/flexslider.css",
        "/src/app/plugins/calender/fullcalendar.min.css",
        "/src/app/plugins/animate.css",
        "/src/app/plugins/pop-up/magnific-popup.css",
        "/src/app/plugins/rs-plugin/css/settings.css",
        "/src/app/plugins/owl-carousel/owl.carousel.css" ,
//        <!-- GOOGLE FONT -->
        "https://fonts.googleapis.com/css?family=Open+Sans:400,600,600italic,400italic,700",
        "https://fonts.googleapis.com/css?family=Roboto+Slab:400,700",
//        "/src/app/plugins/options/optionswitch.css",

//        <!-- CUSTOM CSS -->
        "/src/app/css/style.css",
        "/src/app/css/default.css",
    ];
    public $js = [
        "/src/app/plugins/jquery/jquery-1.11.1.min.js",
        "/src/app/plugins/bootstrap/js/bootstrap.min.js",
        "/src/app/plugins/flexslider/jquery.flexslider.js",
        "/src/app/plugins/rs-plugin/js/jquery.themepunch.tools.min.js",
        "/src/app/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js",
        "/src/app/plugins/selectbox/jquery.selectbox-0.1.3.min.js",
        "/src/app/plugins/pop-up/jquery.magnific-popup.js",
        "/src/app/plugins/animation/waypoints.min.js",
        "/src/app/plugins/count-up/jquery.counterup.js",
        "/src/app/plugins/animation/wow.min.js",
        "/src/app/plugins/animation/moment.min.js",
        "/src/app/plugins/calender/fullcalendar.min.js",
        "/src/app/plugins/owl-carousel/owl.carousel.js",
        "/src/app/plugins/timer/jquery.syotimer.js",
        "/src/app/plugins/smoothscroll/SmoothScroll.js",
        "/src/app/js/custom.js",
//        "/src/app/plugins/options/optionswitcher.js",
    ];

}

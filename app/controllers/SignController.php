<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 16.03.2019
 * Time: 22:00
 */

namespace app\controllers;


use app\models\LoginForm;
use app\models\SignUp;
use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;


/**
 * Class SignController
 * @package app\controllers
 */
class SignController extends Controller
{
    /**
     * @var string
     */
    public $layout = '/sign';

    /**
     * @return string|Response
     */
    public function actionIn()
    {

        if (!Yii::$app->user->isGuest)
            return $this->redirect(Url::to(["/cpanel/default/index"]));

        $model = new LoginForm();

        if (Yii::$app->request->isPost) {

            $model->load(Yii::$app->request->post(), "LoginForm");

            if ($model->validate() && $model->login()) {
                Yii::$app->session->setFlash('info', Yii::t('app', 'Welcome {user}', ['user' => Yii::$app->user->identity->fullname]));
                return $this->redirect("/cpanel/default/index");
            } else {
                return $this->render("/sign/in", ['model' => $model]);
            }
        }
        return $this->render('in', ['model' => $model]);

    }

    /**
     * @return string|Response
     * @throws Exception
     */
    public function actionUp($id = false)
    {
        $model = new SignUp();

        if ($post = Yii::$app->request->post()) {
            if ($model->load($post) && $model->validate() && $model->signup()) {

                Yii::$app->session->setFlash('success', 'You are successfully registered.');
                return $this->redirect("/sign/in");

            } else
                return $this->render("/sign/up", ['model' => $model]);
        }

        return $this->render('up', [
            'model' => $model
        ]);
    }


    /**
     * @return Response
     */
    public function actionOut()
    {
        Yii::$app->user->logout();
        return $this->redirect(Url::to(['/sign/in']));
    }

    /**
     * @return string
     */
    public function actionLock()
    {
        return $this->render("lock");
    }

}
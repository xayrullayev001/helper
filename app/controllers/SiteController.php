<?php


namespace app\controllers;


use Yii;
use yii\web\Controller;

class SiteController extends Controller
{
    public function actionError()
    {
        $this->layout = '/empty';

        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            return $this->render('error', ['exception' => $exception]);
        }
    }

}
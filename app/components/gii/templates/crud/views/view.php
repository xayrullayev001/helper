<?php

use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = $model-><?= $generator->getNameAttribute() ?>;
?>
<div class="row">
    <div class="col s12 m12 l12">
        <div class="row">
            <div class="card">
                <div class="card-content">
                    <div class="row" style="margin-bottom: 0px;">
                        <div class="page-title">
                            <?= Html::encode($this->title) ?>
                        </div>
                        <div class="page-title-buttons">
                            <?= "<?= " ?>Html::a(<?= $generator->generateString('Update') ?>,['update', <?= $urlParams ?>], ['class' => 'btn btn-primary']) ?>
                            <?= "<?= " ?>Html::a(<?= $generator->generateString('Delete') ?>,['delete', <?= $urlParams ?>], [
                            'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => <?= $generator->generateString('Are you sure you want to delete this item?') ?>
                                ,'method' => 'post',
                                ],]) ?>
                            <?= "<?= " ?>Html::a(<?= $generator->generateString('Create ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, ['create'], ['class' => 'btn btn-success']) ?>
                            <?= "<?= " ?>Html::a(Yii::t('app', 'Back'), ['index'], ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">


                <?= "<?= " ?>DetailView::widget([
                'model' => $model,
                'attributes' => [
                <?php
                if (($tableSchema = $generator->getTableSchema()) === false) {
                    foreach ($generator->getColumnNames() as $name) {
                        if($name == "id")continue;
                        if($name == "created_at" || $name == "updated_at"){
                            echo "            '" . $name . ":date',\n";
                            continue;
                        }
                        if($name == "image" || $name == "logo" || $name == "picture"){
                            echo "            '" . $name . ":image',\n";
                            continue;
                        }
                        echo "            '" . $name . "',\n";
                    }
                } else {
                    foreach ($generator->getTableSchema()->columns as $column) {
                        $format = $generator->generateColumnFormat($column);
                        if($column->name == "id")continue;
                        if($column->name == "created_at" || $column->name == "updated_at"){
                            echo "            '" . $column->name . ":date',\n";
                            continue;
                        }
                        if($column->name == "image" || $column->name == "logo" || $column->name == "picture"){
                            echo "            '" . $column->name . ":image',\n";
                            continue;
                        }
                        echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                    }
                }
                ?>
                ],
                ]) ?>

            </div>
        </div>
    </div>
</div>


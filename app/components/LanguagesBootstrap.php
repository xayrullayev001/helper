<?php


namespace app\components;
use app\models\Lang;
use yii\base\BootstrapInterface;
use yii\caching\FileCache;

class LanguagesBootstrap implements BootstrapInterface
{

    /**
     * @inheritDoc
     */
    public function bootstrap($app)
    {
        $cache = new FileCache();
        $languages = Lang::find()->all();
        $cache->set('languages',$languages);
    }
}
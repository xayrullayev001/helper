<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 23:38
 */

namespace app\components\auth\controllers;

use app\components\auth\models\AuthRule;
use app\components\auth\ViewAction;
use Yii;
use yii\web\Controller;
use yii\web\ErrorAction;

class RuleController extends Controller
{
    public function actions()
    {
        return [
            'error' => ['class' => ErrorAction::className()],
            'index' => ['class' => ViewAction::className(),
                'params'=>['model' => AuthRule::className()],
            ],
        ];
    }


    public function actionCreate()
    {
        $model = new \app\components\auth\models\AuthRule();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()  && $model->save()) {
                return  $this->redirect("index");
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
}
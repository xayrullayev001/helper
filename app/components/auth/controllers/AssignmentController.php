<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 23:38
 */

namespace app\components\auth\controllers;

use Yii;
use app\components\auth\models\AuthAssignment;
use app\components\auth\models\AuthRule;
use app\components\auth\ViewAction;
use yii\web\Controller;
use yii\web\ErrorAction;
use app\components\auth\AssignmentView;

class AssignmentController extends Controller
{
    public function actions()
    {
        return [
            'error' => ['class' => ErrorAction::className()],
            'index' => ['class' => AssignmentView::className(),
                'params'=>['model' => AuthAssignment::className()],
            ],
        ];
    }



    public function actionCreate()
    {
        $model = new \app\components\auth\models\AuthAssignment();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()  && $model->save()) {
                return  $this->redirect("index");
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionView($item_name, $user_id){
        $manager = Yii::$app->authManager->getPermissionsByRole($item_name);

        $user = Yii::$app->user->identity;

        return $this->render('view',['model'=>$manager]);
    }

}
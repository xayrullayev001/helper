<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 23:38
 */

namespace app\components\auth\controllers;

use app\components\auth\models\AuthItemChild;
use app\components\auth\models\AuthRule;
use app\components\auth\ViewAction;
use yii\base\Exception;
use yii\db\Query;
use yii\helpers\Url;
use yii\rbac\Permission;
use yii\web\Controller;
use yii\web\ErrorAction;
use Yii;
use yii\web\UnsupportedMediaTypeHttpException;

class ItemChildController extends Controller
{

    public function actions()
    {
        return [
            'error' => ['class' => ErrorAction::className()],
            'index' => ['class' => ViewAction::className(),
                'params' => ['model' => AuthItemChild::className()],
            ],
        ];
    }


    public function actionCreate()
    {
        $model = new \app\components\auth\models\AuthItemChild();

        if ($post = Yii::$app->request->post('AuthItemChild')) {
            $authManager = Yii::$app->authManager;
            if (is_array($post['child'])) {
                for ($i = 0; $i < count($post['child']); $i++) {
                    if ($authManager->canAddChild(
                        $authManager->createRole($post['parent']),
                        $authManager->createPermission($post['child'][$i]))) {
                        try {
                            $authManager->addChild($authManager->createRole($post['parent']),
                                $authManager->createPermission($post['child'][$i]));
                        } catch (Exception $e) {
                            throw $e;
                        }
                    }
                }
            } else if (Yii::$app->authManager->canAddChild(
                $authManager->createRole($post['parent']),
                $authManager->createPermission($post['child']))) {
                try {
                    $authManager->addChild(Yii::$app->authManager->createRole($post['parent']),
                        $authManager->createPermission($post['child'][$i]));
                } catch (Exception $e) {
                    throw $e;
                }
            }
            return $this->redirect("index");
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionView($parent, $child)
    {
        $authManager = Yii::$app->getAuthManager();
        if ($authManager instanceof yii\rbac\DbManager) {

            $query = (new Query())->from($authManager->itemChildTable)
                ->where(['parent' => $parent, 'child' => $child])
                ->one($authManager->db);
            return $this->render('view', ['model' => $query]);
        } else
            throw new UnsupportedMediaTypeHttpException("This function not support type of " . $authManager::className());

    }

    public function actionDelete($parent, $child)
    {
        $authManager = Yii::$app->getAuthManager();
        if ($authManager instanceof yii\rbac\DbManager) {
            $parent = $authManager->getRole($parent);
            $child = $authManager->getPermission($child);
            $authManager->removeChild($parent, $child);
            return $this->redirect("/auth/item-child/index");
        } else
            throw new UnsupportedMediaTypeHttpException("This function not support type of " . $authManager::className());

    }


}
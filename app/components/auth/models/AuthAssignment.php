<?php

namespace app\components\auth\models;

use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "auth_assignment".
 *
 * @property string $item_name
 * @property string $user_id

 *
 * @property AuthItem $itemName
 */
class AuthAssignment extends \yii\db\ActiveRecord
{

    public $PERMISSION = "Assignment-index";


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_assignment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'item_name'], 'required'],

            [['item_name', 'user_id'], 'string', 'max' => 64],
            [['item_name', 'user_id'], 'unique', 'targetAttribute' => ['item_name', 'user_id']],
            [['item_name'], 'exist', 'skipOnError' => true, 'targetClass' => AuthItem::className(), 'targetAttribute' => ['item_name' => 'name']],
        ];
    }

    /**
    * @return array
    */
    public function dropList()
    {
        return ArrayHelper::map(self::find()->asArray()->all(),'item_name', 'user_id');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'item_name' => Yii::t('app', 'Item Name'),
 
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemName()
    {
        return $this->hasOne(AuthItem::className(), ['name' => 'item_name']);
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Auth assignment';
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'Assign roles to users';
    }

    public function getUsername()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'user_id']);
    }
}

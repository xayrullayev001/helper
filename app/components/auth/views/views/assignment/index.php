<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 23:41
 */

use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Roles');

?>
<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="row" style="margin-bottom: 0px;">
                    <div class="page-title">
                        <?= Html::encode($this->title) ?>
                    </div>
                    <div class="page-title-buttons">
                        <?php
                        echo Html::a(Yii::t("app", "Create"), ['/auth/assignment/create'], ['class' => 'btn btn-primary']);
                        echo Html::a(Yii::t('app', 'Back'), ['/auth'], ['class' => 'btn btn-primary']);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <?php
                echo
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        'item_name',
                        'user_id',
                        'created_at:datetime',
                        ['class' => ActionColumn::className(),
                            'header' => 'Actions',
                            'template' => '{view} {update}'
                        ]
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
</div>


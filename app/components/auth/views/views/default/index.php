<?php

use yii\gii\model;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $models model[] */
/* @var $content string */

$models = Yii::$app->controller->module->models;
$this->title = 'Welcome to Auth';

?>

<div class="default-index">
    <div class="page-header">
        <h1>Welcome to Auth <small> for all users roles and authorities</small></h1>
    </div>
    <p class="lead">Start the fun with the following modules:</p>
    <div class="row">
        <div class="row">
            <?php foreach ($models as $id => $model): ?>
                <div class="col s4 m4">
                    <div class="card white darken-1">
                        <div class="card-content">
                            <span class="card-title"><?= Html::encode($model->getName()) ?></span>
                            <p><?= $model->getDescription() ?></p>
                        </div>
                        <div class="card-action">
                            <?= Html::a('Start &raquo;', [$id . '/index'], ['class' => 'btn btn-default']) ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 23:56
 */

use yii\helpers\Html;

$this->title = Yii::t('app', 'Create item child');
?>


<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="row" style="margin-bottom: 0px;">
                    <div class="page-title">
                        <?= Html::encode($this->title) ?>
                    </div>
                    <div class="page-title-buttons">
                        <?= Html::a(Yii::t("app", "Back"), ['index'], ['class' => 'btn btn-primary']); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <?= $this->render('form/_form', [
                    'model' => $model,
                ]) ?>

            </div>
        </div>
    </div>
</div>


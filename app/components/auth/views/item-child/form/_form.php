<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\auth\models\AuthItem;
use yii\helpers\ArrayHelper;
use yii\rbac\Permission;

/* @var $this yii\web\View */
/* @var $model \app\components\auth\models\AuthItemChild */
/* @var $form ActiveForm */

?>
<div class="form-_form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent')->dropDownList(
        AuthItem::find()
            ->where(['type' => Permission::TYPE_ROLE])
            ->select(['name', 'name'])
            ->indexBy('name')
            ->column()

    , ['prompt' => 'Select Survey Type', 'id' => 'Survey_Type_dropdown']) ?>


    <?= $form->field($model, 'child')->dropDownList(
        ArrayHelper::map(AuthItem::find()->where(['type' => Permission::TYPE_PERMISSION])->all(), 'name', 'name', null)
    , ["multiple" => "multiple"]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- form-_form -->

<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 23:41
 */

use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Item child');

?>


<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="row" style="margin-bottom: 0px;">
                    <div class="page-title">
                        <?= Html::encode($this->title) ?>
                    </div>
                    <div class="page-title-buttons">
                        <?php
                        echo Html::a(Yii::t("app", "Create"), ['/auth/item-child/create'], ['class' => 'btn btn-primary']);
                        echo Html::a(Yii::t('app', 'Back'), ['/auth'], ['class' => 'btn btn-primary']);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <?php
                echo
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => SerialColumn::className()],
                        [
                            'label' => 'Parent',
                            'format' => 'html',
                            'value' => function ($model) {
                                return Html::a($model['parent'], ['/auth/role/view', 'name' => $model['parent']]);
                            },
                        ],
                        'child',
                        ['class' => ActionColumn::className(),
                            'template' => '{view} {update} {delete}',
                        ],
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{delete}',
                            'buttons' => [
                                'delete' => function ($url, $model, $key) {
                                    $icon = Html::tag('span', 'delete', ['class' => "material-icons"]);
                                    return Html::a($icon, Url::to(['delete', 'parent' => $model['parent'], 'child' => $model['child']]));
                                }
                            ],
                        ],
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
</div>


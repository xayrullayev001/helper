<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 23:41
 */

use yii\helpers\Html;


echo Html::a(Yii::t('app','Create'),['create'],['class'=>'btn btn-primary']);
echo Html::a(Yii::t('app','Back'),['/auth'],['class'=>'btn btn-primary']);
echo
\yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'name',
        'data',
        'created_at:datetime',
        'updated_at:datetime',
        ['class'=>\yii\grid\ActionColumn::className(),
            'header'=>'Actions',
            'template' => '{view} {update}'
        ]
    ]
]);

<?php

use yii\gii\model;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $models model[] */
/* @var $content string */

$models = Yii::$app->controller->module->models;
$this->title = "Welcome to Auth  for all users roles and authorities";

?>

<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="row" style="margin-bottom: 0px;">
                    <div class="page-title">
                        <?= Html::encode($this->title) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12 ">
            <div class="row">
            <div class="card">
                <div class="card-content">
                    <?php foreach ($models as $id => $model): ?>
                        <div class="col s4 m4">
                            <div class="card white darken-1">
                                <div class="card-content">
                                    <span class="card-title"><?= Html::encode($model->getName()) ?></span>
                                    <p><?= $model->getDescription() ?></p>
                                </div>
                                <div class="card-action">
                                    <?= Html::a('Start &raquo;', [$id . '/index'], ['class' => 'btn btn-default']) ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            </div>
        </div>
    </div>

<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 23:56
 */

use yii\helpers\Html;


$this->title = Yii::t('app', 'Item child');


?>


<div class="col s12">
    <div class="card">
        <div class="card-content">
            <?= \yii\bootstrap\Html::encode($this->title) ?>
        </div>
    </div>
</div>
<?php
echo Html::a(Yii::t("app", "Back"), ['index'], ['class' => 'btn btn-primary']);
?>
<div class="col s12 m12 l6">
    <div class="card">
        <div class="card-content">
            <?= $this->render('form/_form', [
                'model' => $model,
            ]) ?>

        </div>
    </div>
</div>


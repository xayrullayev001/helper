<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 06.10.2019
 * Time: 23:19
 */

echo \yii\widgets\DetailView::widget([
    'model' => $model,
    'attributes' => [
        'parent' ,
        'child'
    ]
]);
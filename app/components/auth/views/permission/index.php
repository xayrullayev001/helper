<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 22.09.2019
 * Time: 23:41
 */

use yii\rbac\Permission;
use yii\rbac\Item;
use yii\rbac\Role;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Permissions');
?>

<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="row" style="margin-bottom: 0px;">
                    <div class="page-title">
                        <?= Html::encode($this->title) ?>
                    </div>
                    <div class="page-title-buttons">
                        <?php
                        echo Html::a(Yii::t("app", "Create"), ['/auth/permission/create'], ['class' => 'btn btn-primary']);
                        echo Html::a(Yii::t('app', 'Back'), ['/auth'], ['class' => 'btn btn-primary']);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <?= \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => \yii\grid\SerialColumn::className()],
                        ['label' => 'Name',
                            'value' => function ($model) {
                                return $model['name'];
                            },
                        ],
                        [
                            'label' => 'description',
                            'value' => function ($model) {
                                return $model['description'];
                            },
                        ],
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{delete}',
                            'buttons' => [
                                'delete' => function ($url, $model, $key) {
//                                    <span class="material-icons" title="Delete user data">delete</span>
                                    $icon = Html::tag('span', 'delete', ['class' => "material-icons"]);
                                    return \yii\helpers\Html::a($icon, Url::to(['permission/delete', 'name' => $model['name']]));
                                }
                            ],
                        ],
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>

</div>
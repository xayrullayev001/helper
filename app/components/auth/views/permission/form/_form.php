<?php

use yii\helpers\Html;

?>
<div class="form-_form">

    <?= Html::beginForm('', 'post') ?>

    <? $input = Html::label(Yii::t('app','Name'), 'authitem-name', null, ["class" => "form-control"]);
    $input .= Html::textInput('name', $model ? $model->name : '', ['id' => 'authitem-v', "class" => "form-control"]) ?>
    <?= Html::tag('div', $input, ['class' => 'form-group']) ?>

    <?= Html::hiddenInput('type', \yii\rbac\Permission::TYPE_PERMISSION, ['id' => 'authitem-type', "class" => "form-control"]) ?>

    <?= Html::label(Yii::t('app','Description'), 'authitem-description', null, ["class" => "form-control"]) ?>
    <?= Html::textInput('description', $model ? $model->description : '', ['id' => 'authitem-description', "class" => "form-control"]) ?>
    
    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>

    <?= Html::endForm(); ?>

</div><!-- form-_form -->

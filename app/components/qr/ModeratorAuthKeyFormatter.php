<?php

/*
 * This file is part of the 2amigos/qrcode-library project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\components\qr;

use Da\QrCode\Format\AbstractFormat;

/**
 * Class Youtube formats a string to a valid youtube video link
 *
 * @author Antonio Ramirez <hola@2amigos.us>
 * @link https://www.2amigos.us/
 * @package Da\QrCode\Format
 */
class ModeratorAuthKeyFormatter extends AbstractFormat
{
    /**
     * @var string the video ID
     */
    public $url;

    /**
     * @return string the formatted string to be encoded
     */
    public function getText(): string
    {
        return $this->url;
    }
}

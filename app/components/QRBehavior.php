<?php


namespace app\components;


use yii\base\Event;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

class QRBehavior extends AttributeBehavior
{
    public $qr_code;

    public function events()
    {
        return [
            'class' => AttributeBehavior::className(),
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['qr_code'],
            ],
            'value' => function () {
                return $this->generate();
            }
        ];
    }

    public function generate(Event $event)
    {
        $this->qr_code = "QRGenerateBeforeValidate";
        return $this->qr_code;

        //use Da\QrCode\Format\PhoneFormat;
        ////                $format = new PhoneFormat(['phone' => +998941727439]);
        //$format = new Da\QrCode\Format\YoutubeFormat(['videoId' => "https://www.youtube.com/watch?v=yDmEf8knlKQ"]);
        ////                $format = new \app\components\qr\SaleFormat(['product'=>'thumbnails']);
        //$qrCode = new QrCode($format);
        //$qrCode->writeFile("uploads/products/qrcode.png");
//            <!--<img src="/uploads/products/qrcode.png" alt="">-->
    }
}
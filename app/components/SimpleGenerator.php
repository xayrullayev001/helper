<?php


namespace app\components;


use Yii;
use yii\gii\CodeFile;

class SimpleGenerator
{
    public $template = "@app/components/templates";
    private $controller = "";
    private $model = "";
    private $urlParsed = [];

    /**
     * SimpleGenerator constructor.
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->setUrlParsed($url);
    }


    public function generateController()
    {
        $urlParsed = $this->getUrlParsed();

        $controllerFile = "app/modules/" . $urlParsed[1] . "/controllers/" . ucfirst($urlParsed[2]) . "Controller.php";
        $actionFile = "app/modules/" . $urlParsed[1] . "/views/".strtolower($urlParsed[2])."/" . strtolower($urlParsed[3]) . ".php";

        if (!file_exists($controllerFile)) {
            $namespace = "app\modules\\" . $urlParsed[1] . "\controllers";
            $controllerClass = ucfirst($urlParsed[2]) . "Controller";
            $action = $urlParsed[3];

            $files[] = new CodeFile(
                $controllerFile,
                Yii::$app->controller->renderPartial($this->getTemplate() . "/controller", [
                    'namespace' => $namespace,
                    'controllerClass' => $controllerClass,
                    'action' => $action
                ])
            );
            $files[] = new CodeFile(
                $actionFile,
                Yii::$app->controller->renderPartial($this->getTemplate() . "/index", [
                    'controller' => strtolower($urlParsed[2]),
                ])
            );
            foreach ($files as $file)$file->save();
        }

        return true;
    }

    public function generateModel($url)
    {
        $urlParsed = explode("/", $url);

        $controllerFile = "app/modules/" . $urlParsed[1] . "/controllers/" . ucfirst($urlParsed[2]) . "Controller.php";
        if (file_exists($controllerFile)) {
            $namespace = "app\modules\\" . $urlParsed[1] . "\controllers";
            $controllerClass = ucfirst($urlParsed[2]) . "Controller";

            $content = Yii::$app->controller->renderPartial($this->getTemplate() . "/controller", [
                'namespace' => $namespace,
                'controllerClass' => $controllerClass,
                'modelClass' => $controllerClass
            ]);
            $file = new CodeFile(
                $controllerFile,
                $content
            );
            $file->save();
        }

        return true;
    }


    /**
     * @return array
     */
    public function getUrlParsed(): array
    {
        return $this->urlParsed;
    }

    /**
     * @param array $urlParsed
     */
    public function setUrlParsed(string $url)
    {
        $urlParsed = explode("/", $url);
        $this->urlParsed = $urlParsed;
    }

    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * @param string $template
     */
    public function setTemplate(string $template)
    {
        $this->template = $template;
    }


}
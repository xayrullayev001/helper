<?php echo "<?php\n"?>

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

<?php echo '$this->title'?> = Yii::t('app', '<?php echo $controller?>');

<?php echo '?>'?>

<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="row" style="margin-bottom: 0px;">
                    <div class="page-title">
                        <?php echo '<?='?>  Html::encode($this->title) ?>
                    </div>
                    <div class="page-title-buttons">
                        <?php echo'<?='?> Html::a(Yii::t('app', 'Create <?php echo $controller?>'), ['create'], ['class' => 'btn btn-success ']) ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                Content
            </div>
        </div>
    </div>
</div>

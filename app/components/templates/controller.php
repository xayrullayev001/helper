<?php
/**
 * This is the template for generating a controller class file.
 */

use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\controller\Generator */
/* @var $namespace string */
/* @var $urlParsed array */

echo "<?php\n";
?>

namespace <?= $namespace ?>;

<?php echo"use Yii;\n";?>
<?php echo"use yii\web\Controller;\n";?>
<?php echo"use yii\\filters\AccessControl;\n";?>
<?php echo"use yii\\filters\VerbFilter;\n";?>
<?php echo"use yii\web\ForbiddenHttpException;\n";?>

class <?= StringHelper::basename($controllerClass) ?> extends Controller
{

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                    'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'pageCache' => [
                'class' => 'yii\filters\PageCache',
                'only' => ['index','view'],
                'duration' => 5,
                'varyByRoute' => true,
                'variations' => [
                    \Yii::$app->language,
                    Yii::$app->getUser()->getId()
                ]
            ],
        ];
    }

    /**
     * @return string
     * @throws ForbiddenHttpException
     */
    public function action<?=ucfirst($action)?>()
    {
        return $this->render('<?=$action?>' );
    }

}

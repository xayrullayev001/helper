<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 06.06.2019
 * Time: 11:16
 */

namespace app\components;

use app\models\ExchangeSearch;
use Yii;
use yii\base\Exception;
use yii\base\InvalidValueException;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;


/**
 * Class CRUDController
 * @package app\components
 */
class CRUDController extends \yii\web\Controller
{
    /**
     * @var
     */
    public $model;
    /**
     * @var
     */
    public $image;
    private $modelClassName = "\\app\\models\\";

    public function init()
    {
        $this->modelClassName .= $this->model;
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'pageCache' => [
                'class' => 'yii\filters\PageCache',
                'only' => ['index','view'],
                'duration' => 2,
                'varyByRoute' => true,
                'dependency' => [
                    'class' => 'yii\caching\DbDependency',
                    'sql' => 'SELECT SUM(updated_at) FROM ' . $this->modelClassName::tableName(),
                ],
                'variations' => [
                    \Yii::$app->language,
                    Yii::$app->getUser()->getId()
                ]
            ],
        ];
    }

    /**
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        $m = '\\app\\models\\' . $this->model . 'Search';
        $searchModel = new $m();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws \yii\base\Exception
     * @throws \Exception
     */
    public function actionCreate()
    {

        $m = '\\app\\models\\' . $this->model;
        $model = new $m;
        if ($model->load(Yii::$app->request->post()) && $this->fileUpload($model, $this->image, strtolower($this->model)) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else if ($model->hasErrors()) {

            throw new \InvalidArgumentException("{$this->model} not loaded", 409);

        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $model
     * @param $image
     * @param $uploadFileName
     * @return bool|void
     * @throws Exception
     * @throws UserException
     */
    protected function fileUpload($model, $image, $uploadFileName)
    {
        if ($model == false || $image == false) return true;
        $dir = new \yii\helpers\BaseFileHelper();
        $files = \yii\web\UploadedFile::getInstance($model, $image);
        if ($files == null) {
            $model->{$image} = $model->oldAttributes["{$image}"];
            return true;
        }
        $name = Yii::$app->security->generateRandomString(12);
        $uploadPath = 'uploads/' . $uploadFileName;
        $dir->createDirectory($uploadPath);
        $isUpload = $files->saveAs($uploadPath . '/' . $name . '.' . $files->extension);
        $model->{$image} = '/' . $uploadPath . '/' . $name . '.' . $files->extension;
        if (!$model->isNewRecord && file_exists(Yii::getAlias('@webroot') . $model->oldAttributes[$image])) {
            @unlink(Yii::getAlias('@webroot') . $model->oldAttributes[$image]);
        }

        if ($isUpload) {
            return true;
        } else {
            throw new InvalidValueException("File not uploaded", 409, $model->getErrors());

        }
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $this->fileUpload($model, $this->image, strtolower($this->model)) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);

    }

    /**
     * Finds the Model model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed $model the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $m = '\\app\\models\\' . $this->model;
        if (($model = $m::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Displays a single Transmission model.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);

    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($this->image && file_exists(Yii::getAlias('@webroot') . $model->{$this->image})) {
            @unlink(Yii::getAlias('@webroot') . $model->{$this->image});
        }
        $model->delete();

        return $this->redirect(['index']);

    }


}
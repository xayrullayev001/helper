<?php
/**
 * Created by PhpStorm.
 * User: Behzod
 * Date: 11.12.2019
 * Time: 7:56
 */

namespace app\widgets;

use Yii;
use yii\base\Widget;
/**
 * echo \app\widgets\Toast::widget([
 *   'options'=>[
 *       'success'=>'teal',
 *       'error'=>'deep-orange darken-3',
 *       'warning'=>'amber accent-4',
 *       'info'=>'blue'
 *    ]
 *  ]);
 */
class Toast extends Widget
{
    /**
     * @var array
     */
    public $options = [];

    public function init()
    {
    }

    /**
     * @return bool
     */
    public function run()
    {
        $this->notifyMessages();
    }

    /**
     * @param array $messages
     */
    public function notifyMessages(){
        $action = \Yii::$app->view;
        foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
            $action->
            registerJs('setTimeout(function () {
            Materialize . toast("'.$message.'", 4000,"'.$this->options[$key].'") }, 2000);');
        }
    }
    
    public function registerClientScripts()
    {
//        ToastAsset::register(Yii::$app->controller->action->id);
    }
}
<?php


namespace app\widgets\scan;


use yii\web\AssetBundle;

class ScannerAsset extends AssetBundle
{

    public $sourcePath = "@app/widgets/scan";
    public $js = [];
    public $css = [];
    public $depends = [];
}
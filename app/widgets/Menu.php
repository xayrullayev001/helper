<?php


namespace app\widgets;


use app\models\Menus;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class Menu
 * @package app\widgets
 *
 *
 * * ```php
 * echo Menu::widget([
 *      'options' => [
 *          'class' => 'sidebar-menu collapsible collapsible-accordion'
 *      ],
 *      'activateParents'=> true,
 *      'activeCssClass' => 'active',
 *      'itemOptions' => ['class' => 'no-padding'],
 *      'linkTemplate' => '<a href="{url}" class="waves-effect waves-grey" ><i class="material-icons">subject</i> {label}</a>',
 *      'submenuTemplate' => "\n<div class='collapsible-body' role='menu'>\n<ul>{items}\n</ul></div>\n",
 *     'items' => [
 *         // Important: you need to specify url as 'controller/action',
 *         // not just as 'controller' even if default action is used.
 *         ['label' => 'Home', 'url' => ['site/index']],
 *         // 'Products' menu item will be selected as long as the route is 'product/index'
 *         ['label' => 'Products', 'url' => ['product/index'], 'items' => [
 *             ['label' => 'New Arrivals', 'url' => ['product/index', 'tag' => 'new']],
 *             ['label' => 'Most Popular', 'url' => ['product/index', 'tag' => 'popular']],
 *         ]],
 *         ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
 *     ],
 * ]);
 * ```
 *
 *  <?= \app\widgets\Menu::widget([
 * 'items' => [
 *
 * // Important: you need to specify url as 'controller/action',
 * // not just as 'controller' even if default action is used.
 * [
 * 'label' => 'Home', 'url' => ['site/index'],
 * ],
 * // 'Products' menu item will be selected as long as the route is 'product/index'
 * [
 * 'label' => 'Products',
 * 'url' => ['#'],
 * 'template' => '<a class="collapsible-header waves-effect waves-grey" ><i class="material-icons">{icon}</i> {label}</a>',
 * 'class'=>'collapsible-header waves-effect waves-grey',
 * 'icon' => 'explicit',
 * 'items' => [
 * [
 * 'label' => 'New Arrivals', 'url' => ['product/index', 'tag' => 'new'], 'icon' => 'explicit'
 * ],
 * [
 * 'label' => 'Most Popular', 'url' => ['product/index', 'tag' => 'popular'],
 * 'linkTemplate' => '<a class="waves-effect waves-grey" ><i class="material-icons">{icon}</i> {label}</a>'
 * ],
 * ]
 * ],
 * [
 * 'label' => 'Login', 'url' => ['site/login'],
 * 'linkTemplate' => '<a class="waves-effect waves-grey" ><i class="material-icons">subject</i> {label}</a>',
 * 'visible' => Yii::$app->user->isGuest
 * ],
 * ],
 * ]); ?>
 */
class Menu extends \yii\widgets\Menu
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public function init()
    {

        $this->options['class'] = 'sidebar-menu collapsible collapsible-accordion';
        $this->activateParents = true;
        $this->activeCssClass = 'active';
        $this->itemOptions ['class'] = 'no-padding';
        $this->linkTemplate = '<a href="{url}" class="{class}" ><i class="material-icons">{icon}</i> {label}</a>';
        $this->submenuTemplate = "\n<div class='collapsible-body' role='menu'>\n<ul>{items}\n</ul></div>\n";
        $menus = Menus::find()->where(['parent_id'=>Menus::MENU_PARENT_NOT])->orderBy(['sort_order'=>SORT_ASC])->with('withChilds')->all();
        $this->generateMenu($menus,$this->items);
        parent::init();
    }
    private function generateMenu($menus, &$arrays)
    {
        foreach ($menus as $menu){
            $visible = $menu->permission === Menus::PUBLIC ? true : Yii::$app->user->can("{$menu->permission}");
            if(is_null($menu->withChilds)){
                array_push($arrays, [
                    'label' => Yii::t('app', $menu->label),
                    'url' => [$menu->url],
                    'icon' => $menu->icon,
                    'class' => $menu->class,
                    'linkTemplate' => $menu->link_template,
                    'visible'=>$visible
                ]);
            }else {
                $withParent = [
                    'label' => Yii::t('app', $menu->label),
                    'url' => [$menu->url],
                    'class' => $menu->class,
                    'icon' => $menu->icon,
                    'linkTemplate' => $menu->link_template,
                    'visible'=>$visible,
                    'items' => [],
                ];

                $this->generateMenu($menu->withChilds,$withParent['items']);
                array_push($arrays, $withParent);
            }
        }
    }

//    private function generateMenuItems(&$arrays, $parentId = false)
//    {
//        $menus = Menus::find()
//            ->where(['parent_id' => $parentId, 'status' => self::STATUS_ACTIVE])
//            ->orderBy(['sort_order' => SORT_ASC])->all();
//
//
//        $ar = [];
//        foreach ($menus as $menu) {
//
//            $visible = $menu->permission === Menus::IS_PUBLIC ? true : Yii::$app->user->can("{$menu->permission}");
//            if ($menu->has_child === false) {
//                array_push($arrays, [
//                    'label' => Yii::t('app', $menu->label),
//                    'url' => [$menu->url],
//                    'icon' => $menu->icon,
//                    'class' => $menu->class,
//                    'linkTemplate' => $menu->link_template,
//                    'visible'=>$visible
//                ]);
//            } else {
//                $withParent = [
//                    'label' => Yii::t('app', $menu->label),
//                    'url' => [$menu->url],
//                    'class' => $menu->class,
//                    'linkTemplate' => $menu->link_template,
//                    'visible'=>$visible,
//                    'items' => [],
//                ];
//
//                $this->generateMenuItems($withParent['items'], $menu->id);
//                array_push($arrays, $withParent);
//            }
//
//        }
//    }

    /**
     * Renders the content of a menu item.
     * Note that the container and the sub-menus are not rendered here.
     * @param array $item the menu item to be rendered. Please refer to [[items]] to see what data might be in the item.
     * @return string the rendering result
     */
    protected function renderItem($item)
    {

        if (isset($item['linkTemplate'])) {
            $template = ArrayHelper::getValue($item, 'template', $item['linkTemplate']);
            $temp = [];
            $temp['{url}'] = (isset($item['url'])) ? Html::encode(Url::to($item['url'])) : "";

            $temp['{label}'] = (isset($item['label'])) ? $item['label'] : "";

            $temp['{class}'] = (isset($item['class']) && !is_null($item['class'])) ? $item['class'] : "";

            $temp['{icon}'] = (isset($item['icon'])) ? $item['icon'] : "";

            $template = strtr($template, $temp);
            unset($temp);
            return $template;
        } else
            if (isset($item['url'])) {
                $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);
                return strtr($template, [
                    '{url}' => Html::encode(Url::to($item['url'])),
                    '{label}' => $item['label']
                ]);
            }

        $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);

        return strtr($template, [
            '{label}' => $item['label']
        ]);
    }


}
<?php


namespace app\widgets;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class MenuIterator extends \yii\widgets\Menu
{

    public function init()
    {
        $this->options['class'] = 'sidebar-menu collapsible collapsible-accordion';
        $this->activateParents = true;
        $this->activeCssClass = 'active';
        $this->itemOptions ['class'] = 'no-padding';
        $this->linkTemplate = '<a href="{url}" class="waves-effect waves-grey" ><i class="material-icons">subject</i> {label}</a>';
        $this->submenuTemplate = "\n<div class='collapsible-body' role='menu'>\n<ul>{items}\n</ul></div>\n";

        parent::init();
    }

    /**
     * Renders the content of a menu item.
     * Note that the container and the sub-menus are not rendered here.
     * @param array $item the menu item to be rendered. Please refer to [[items]] to see what data might be in the item.
     * @return string the rendering result
     */
    protected function renderItem($item)
    {

        if (isset($item['linkTemplate'])) {
            $template = ArrayHelper::getValue($item, 'template', $item['linkTemplate']);
            $temp = [];
            $temp['{url}'] = (isset($item['url'])) ? Html::encode(Url::to($item['url'])) : "";

            $temp['{label}'] = (isset($item['label'])) ? $item['label'] : "";

//            $temp['{class}'] = (isset($item['class'])) ? $item['class'] : "";

            $temp['{icon}'] = (isset($item['icon'])) ? $item['icon'] : "";

            $template = strtr($template, $temp);
            unset($temp);
            return $template;
        } else
            if (isset($item['url'])) {
                $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);
                return strtr($template, [
                    '{url}' => Html::encode(Url::to($item['url'])),
                    '{label}' => $item['label']
                ]);
            }

        $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);

        return strtr($template, [
            '{label}' => $item['label']
        ]);
    }
}
<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$basePath = dirname(__DIR__);
$webroot = dirname($basePath);

$config = [
    'id' => 'HELPER',
    'name' => 'HELPER',
    'version' => '1.0.0',
    'basePath' => $basePath,
    'bootstrap' => ['log', 'auth', '\\app\\components\\LanguagesBootstrap'],
    'runtimePath' => $webroot . '/runtime',
    'vendorPath' => $webroot . '/vendor',
    'homeUrl' => '/',
    'defaultRoute' => '/sign/in',
    'timeZone' => 'Asia/Tashkent',
    'language' => 'uz',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'modules' => [
        'cpanel' => [
            'class' => 'app\modules\cpanel\CPanel',
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module',
            'downloadAction' => 'gridview/export/download',
        ],
        'simple' => [
            'class' => 'app\modules\simple\SimpleGenerator',
            'layout'=>'/cpanel'
        ],
        /**
         * for auth module
         */
        'auth' => [
            'id' => 'auth',
            'class' => 'app\components\auth\Auth',
            'layout' => '/cpanel',
            'layoutPath' => '@app/views/layouts',
            'viewPath' => '@app/components/auth/views'
        ],
    ],
    'components' => [
        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\DbMessageSource',
                ],
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'IBHTovhyglptXVlptgyrIzFfi-2U7f4l',
            'class' => 'yii\web\Request',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['/sign/in'],
        ],
        'errorHandler' => [
            'errorAction' => '/site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'maxFileSize' => 1024 * 3,
                    'maxLogFiles' => 10
                ],
            ],
        ],
        "authManager" => [
            "class" => '\yii\rbac\DbManager',
//            "defaultRoles" => ["guest"],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
//            'enableLocaleUrls' => true,
//            'enableDefaultLanguageUrlCode' => true,
            'class' => 'yii\web\UrlManager',
//            'languages' => ['uz', 'en', 'ru'],
            'rules' => [
                'sign/in' => '/sign/in',
                'sign/up/<id:[\w\-]+>' => '/sign/up',
                'sign/up' => '/sign/up',
                'sign/out' => '/sign/out',
                'profile' => '/my/profile',
                '<_m:[\w\-]+>/<controller:[\w\-]+>/<action:[\w\-]+>/<id:\w+>' => '<_m>/<controller>/<action>',
                '<_m:[\w\-]+>/<controller:[\w\-]+>/<action:[\w\-]+>/<slug:[\w\-]+>' => '<_m>/<controller>/<action>',
                '<_m:[\w\-]+>/<controller:[\w\-]+>/<action:[\w\-]+>' => '<_m>/<controller>/<action>',
                '<_m:[\w\-]+>/<controller:[\w\-]+>' => '<_m>/<controller>/index',
                '<controller:[\w\-]+>/<action:[\w\-]+>/<id:\w+>' => '<controller>/<action>',
                '<controller:[\w\-]+>/<action:[\w\-]+>/<slug:[\w\-]+>' => '<controller>/<action>',
                '<controller:[\w\-]+>/<action:[\w\-]+>' => '<controller>/<action>',
                '<controller:[\w\-]+>' => '<controller>/index',

            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        // 'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        // 'allowedIPs' => ['127.0.0.1', '::1'],
        'generators' => [
            'alpha_crud' => [
                'class' => 'app\components\gii\crud\Generator',
                'templates' => [
                    'crud' => '@app/components/gii/templates/crud',
                ]
            ],
            'alpha_form' => [
                'class' => 'app\components\gii\form\FormGenerator',
                'templates' => [
                    'form' => '@app/components/gii/templates/form/default',
                ]
            ],
            'alpha_blank' => [
                'class' => 'app\components\gii\blank\BlankGenerator',
                'templates' => [
                    'blank' => '@app/components/gii/templates/blank',
                ]
            ]
        ]
    ];
}

return $config;

<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
//    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
//        'sms' => [
//            'class' => '\app\components\sms\SmS',
//            'baseUrl' => 'https://notify.eskiz.uz/api',
//            'email' => 'dilshod_0309@mail.ru',
//            'password' => 'ETtcrG18voKk3f3xc6COEtp9xK2SbNESGrHGlBkW'
//        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info'],
//                    'maxFileSize' => 1024 * 3,
//                    'maxLogFiles' => 10
                ],
                [
                    'class' => 'yii\log\FileTarget', //в файл
                    'levels' => ['error', 'warning', 'info'],
                    'categories' => ['sms'], //категория логов
                    'logFile' => '@runtime/logs/sms.log', //куда сохранять
                    'logVars' => ['_GET', '_POST'], //добавлять в лог глобальные переменные ($_SERVER, $_SESSION...)
//                    'maxFileSize' => 1024 * 2,
//                    'maxLogFiles' => 20,
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
